/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jmonkey.export;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ti
 */
public class RegistryPropertyExceptionTest {
    
    public RegistryPropertyExceptionTest() {
    }
    
    /**
     * Test of getGroup method, of class RegistryPropertyException.
     */
    @Test
    public void testGetGroup() {
        System.out.println("getGroup");
        RegistryPropertyException instance = new RegistryPropertyException("message", "group", "key");
        String expResult = "group";
        String result = instance.getGroup();
        assertEquals(expResult, result);
     
        System.out.println("getNullGroup");
        instance = new RegistryPropertyException("message", null, "key");
        expResult = null;
        result = instance.getGroup();
        assertEquals(expResult, result);
    }

    /**
     * Test of getKey method, of class RegistryPropertyException.
     */
    @Test
    public void testGetKey() {
        System.out.println("getKey");
        RegistryPropertyException instance = new RegistryPropertyException("message", "group", "key");
        String expResult = "key";
        String result = instance.getKey();
        assertEquals(expResult, result);
        
        System.out.println("getNullKey");
        instance = new RegistryPropertyException("message", "group", null);
        expResult = null;
        result = instance.getKey();
        assertEquals(expResult, result);
    }

    /**
     * Test of getMessage method, of class RegistryPropertyException.
     */
    @Test
    public void testGetMessage() {
        System.out.println("getMessage");
        RegistryPropertyException instance = new RegistryPropertyException("message", "group", "key");
        String expResult = "message";
        String result = instance.getMessage();
        assertEquals(expResult, result);
        
        System.out.println("getNullMessage");
        instance = new RegistryPropertyException(null, "group", "key");
        expResult = null;
        result = instance.getMessage();
        assertEquals(expResult, result);
    }
}
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jmonkey.export;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ti
 */
public class RegistryImplTest {
    
    public RegistryImplTest() {
    }
    
    /**
     * Test of setFile method, of class RegistryImpl.
     */
    @Test
    public void testSetFile() throws IOException {
        System.out.println("setFile");
        File file = null;
        int[] version = {0,0,0};
        RegistryImpl instance = null;
        
        try {
            instance = new RegistryImpl(file, version); 
        } catch (IOException e) {
            // Valid exception.
        } catch (Exception e) {
            fail("Invalid exception thrown.");
        }
        
        file = new File("testSetFile.txt");
        instance = new RegistryImpl(file, version);
        instance.setFile(file);
        assertTrue(instance.getFile() == file );
        file = new File("testSetFile2.txt");
        instance.setFile(file);
        assertTrue(instance.getFile() == file );
        
        System.out.println("PASS");
    }

    /**
     * Test of getFile method, of class RegistryImpl.
     */
    @Test
    public void testGetFile () {
        System.out.println("getFile");
        File f = new File("/tmp/testSetFile2.txt");
        int[] version = {0,0,0};
        try{
            RegistryImpl instance = new RegistryImpl(f, version);
            File result = instance.getFile();
            assertTrue(f == result);
        } catch (IOException e){
            System.out.println(e);
        }
        
        System.out.println("PASS");
    }

    /**
     * Test of getVersion method, of class RegistryImpl.
     */
    @Test
    public void testGetVersion() {
        System.out.println("getVersion - test regular input");
        int[] expResult;
        int[] result;
        int[] version = {0,1,2};
        RegistryImpl reg = new RegistryImpl(version);
        expResult = version;
        result = reg.getVersion();
        assertEquals(expResult, result);
        System.out.println("PASS");
        
        System.out.println("getVersion - test null input");
        version = null;
        reg = new RegistryImpl(version);
        result = reg.getVersion();
        assertNull(result);
        
        System.out.println("PASS");
    }

    /**
     * Test of isAltered method, of class RegistryImpl.
     */
    @Test
    public void testIsAltered() {
        System.out.println("isAltered - test case false");
        int[] version = {0,0,0};
        RegistryImpl instance = new RegistryImpl(version);
        assertFalse(instance.isAltered());
        System.out.println("PASS");
        
        System.out.println("isAltered - test case true");
        File file = new File("/tmp/testSetFile.txt");
        try{
            instance = new RegistryImpl(file, version); 
            instance.setFile(file);          
            file = new File("/tmp/testSetFile2.txt");
            instance.setFile(file);
            assertTrue (instance.isAltered());
        }
        catch (IOException e){
            System.out.println(e);
        }
   
        System.out.println("PASS");
    }

    /**
     * Test of toString method, of class RegistryImpl.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
    }

    /**
     * Test of read method, of class RegistryImpl.
     */
    @Test
    public void testRead() throws Exception {
        RegistryImpl reg = new RegistryImpl(new int[] {0,1,2});
        
        System.out.println("read - test invalid registry file");
        FileReader reader = new FileReader("test\\com\\jmonkey\\export\\testReadInvalid.txt");
        try{
            reg.read(reader);
        } catch (Exception e){
            if (!(e instanceof RegistryFormatException) 
                    || (e.getMessage().equals("not a jmonkey registry file"))){
                fail("Wrong Exception thrown" + e);
            }
        }
        System.out.println("PASS");
        
        System.out.println("read - test valid registry file");
        reader = new FileReader("test\\com\\jmonkey\\export\\testReadValid.txt");
        reg.read(reader);
        
        // Validate that the appropriate groups and
        // properties were created from the valid
        // registry file.
        assert(reg.isGroup("OPTION"));
        assert(reg.isProperty("OPTION", "Paste"));
        assert(reg.getBoolean("OPTION", "Paste"));
        assert(reg.isProperty("OPTION", "Cut"));
        assert (reg.getBoolean("OPTION", "Cut"));
        
        assert(reg.isGroup("COLOURS"));
        assert(reg.isProperty("COLOURS", "Pink"));
        assertEquals(reg.getString("COLOURS", "Pink"), "#FFAFAF");
        assert(reg.isProperty("COLOURS", "Yellow"));
        assertEquals(reg.getString("COLOURS", "Yellow"), "#FFFF00");
        
        System.out.println("read - test null registry file");
        reader = null;
        try{
            reg.read(reader);
        } catch (Exception e){
            if (!(e instanceof RegistryFormatException) 
                    || (e.getMessage().equals("not a jmonkey registry file"))){
                fail("Wrong Exception thrown: "+ e);
            }
        }
        System.out.println("PASS");
    }

    /**
     * Test of write method, of class RegistryImpl.
     */
    @Test
    public void testWrite() throws Exception {
        System.out.println("write");
        
        // Create a test registry with a group "TEST", which
        // contains the boolean property "testProp" set to true.
        RegistryImpl reg = new RegistryImpl(new int[] {0, 1, 2});
        File dataFile = new File("test\\com\\jmonkey\\export\\testWriter.txt");
        reg.setFile(dataFile);
        reg.initGroup("TEST", new String[][]{{"testProp", "true", "boolean"}});
        Writer writer = new FileWriter(dataFile);
        reg.write(writer);
        
        // Read back the created registry file and validate
        // its properties.
        reg.read(new FileReader(dataFile));
        assert(reg.size() == 1);
        assert(reg.isGroup("TEST"));
        assert(reg.isProperty("TEST", "testProp"));
        assert(reg.getBoolean("TEST", "testProp"));
        
        // Try passing null parameters.
        try {
            reg.write(null);
        } catch (IllegalArgumentException ex) {
            // Accepted exception.
        } catch (Exception e) {
            fail("An invalid parameter exception was expected to be thrown.");
        }
    }

    /**
     * Test of isArrayType method, of class RegistryImpl.
     */
    @Test
    public void testIsArrayType() {
        System.out.println("isArrayType");
        RegistryImpl reg = new RegistryImpl(new int[] {0,1,2});
        String group = "TEST";
        String key = "propertyTest";
        reg.setProperty(group, key, "BO@");
        assertFalse(reg.isArrayType(group, key));
        reg.setProperty(group, key, "SA@");
        assertTrue(reg.isArrayType(group, key));
        System.out.println("PASS");
    }

    /**
     * Test of getType method, of class RegistryImpl.
     */
    @Test
    public void testGetType() {
        RegistryImpl reg = new RegistryImpl(new int[] {0,1,2});
        String group = "TEST";
        String key;
        int expResult;
        int result;
        
        System.out.println("getType");
        key = "propertyTest";
        reg.setProperty(group, key, "BO@");
        expResult = RegistryImpl.markerToType("BO@");
        result = reg.getType(group, key);
        assertEquals(expResult, result);
        
        System.out.println("getNullType");
        key = "nullProperty";
        expResult = Registry.TYPE_NONE;
        result = reg.getType(group, key);
        assertEquals(expResult, result);
        
        System.out.println("PASS");
    }

    /**
     * Test of typeToMarker method, of class RegistryImpl.
     */
    @Test
    public void testTypeToMarker() {
        System.out.println("typeToMarker");
        
        assertEquals(RegistryImpl.typeToMarker(Registry.TYPE_STRING_SINGLE), "ST@");
        assertEquals(RegistryImpl.typeToMarker(Registry.TYPE_STRING_ARRAY), "SA@");
        assertEquals(RegistryImpl.typeToMarker(Registry.TYPE_OBJECT_SINGLE),"OB@");
        assertEquals(RegistryImpl.typeToMarker(Registry.TYPE_OBJECT_ARRAY), "OA@");
        assertEquals(RegistryImpl.typeToMarker(Registry.TYPE_BOOLEAN_SINGLE), "BO@");
        assertEquals(RegistryImpl.typeToMarker(Registry.TYPE_BYTE_SINGLE), "BY@");
        assertEquals(RegistryImpl.typeToMarker(Registry.TYPE_BYTE_ARRAY), "BA@");
        assertEquals(RegistryImpl.typeToMarker(Registry.TYPE_CHAR_SINGLE), "CH@");
        assertEquals(RegistryImpl.typeToMarker(Registry.TYPE_CHAR_ARRAY), "CA@");
        assertEquals(RegistryImpl.typeToMarker(Registry.TYPE_SHORT_SINGLE), "SH@");
        assertEquals(RegistryImpl.typeToMarker(Registry.TYPE_INT_SINGLE), "IN@");
        assertEquals(RegistryImpl.typeToMarker(Registry.TYPE_INT_ARRAY), "IA@");
        assertEquals(RegistryImpl.typeToMarker(Registry.TYPE_LONG_SINGLE), "LO@");
        assertEquals(RegistryImpl.typeToMarker(Registry.TYPE_DOUBLE_SINGLE), "DO@");
        assertEquals(RegistryImpl.typeToMarker(Registry.TYPE_FLOAT_SINGLE), "FL@");
        
        try{
            RegistryImpl.typeToMarker(-1);
        } catch (Exception e){
            if (!(e instanceof RegistryException) || !(e.getMessage().startsWith("unknown type"))){
                fail("Wrong type of exception thrown");
            }
        }
        System.out.println("PASS");
    }

    /**
     * Test of markerToType method, of class RegistryImpl.
     */
    @Test
    public void testMarkerToType() {
        System.out.println("markerToType");
        
        assertEquals(RegistryImpl.markerToType("ST@"), Registry.TYPE_STRING_SINGLE);
        assertEquals(RegistryImpl.markerToType("SA@"), Registry.TYPE_STRING_ARRAY);
        assertEquals(RegistryImpl.markerToType("OB@"), Registry.TYPE_OBJECT_SINGLE);
        assertEquals(RegistryImpl.markerToType("OA@"), Registry.TYPE_OBJECT_ARRAY);
        assertEquals(RegistryImpl.markerToType("BO@"), Registry.TYPE_BOOLEAN_SINGLE);
        assertEquals(RegistryImpl.markerToType("BY@"), Registry.TYPE_BYTE_SINGLE);
        assertEquals(RegistryImpl.markerToType("BA@"), Registry.TYPE_BYTE_ARRAY);
        assertEquals(RegistryImpl.markerToType("CH@"), Registry.TYPE_CHAR_SINGLE);
        assertEquals(RegistryImpl.markerToType("CA@"), Registry.TYPE_CHAR_ARRAY);
        assertEquals(RegistryImpl.markerToType("SH@"), Registry.TYPE_SHORT_SINGLE);
        assertEquals(RegistryImpl.markerToType("IN@"), Registry.TYPE_INT_SINGLE);
        assertEquals(RegistryImpl.markerToType("IA@"), Registry.TYPE_INT_ARRAY);
        assertEquals(RegistryImpl.markerToType("LO@"), Registry.TYPE_LONG_SINGLE);
        assertEquals(RegistryImpl.markerToType("DO@"), Registry.TYPE_DOUBLE_SINGLE);
        assertEquals(RegistryImpl.markerToType("FL@"), Registry.TYPE_FLOAT_SINGLE);
    
        try{
            RegistryImpl.markerToType("");
        } catch (Exception e){
            if (!(e instanceof RegistryException)){
                fail("Wrong type of exception thrown");
            }
        }
        System.out.println("PASS");
    }

    /**
     * Test of getString method, of class RegistryImpl.
     */
    @Test
    public void testGetString() {
        System.out.println("getString");
        RegistryImpl reg = new RegistryImpl(new int[] {0,1,2});
        String group = "TEST";
        String key = "property";
        reg.setProperty(group, key, "value");
        
        String expResult = "value";
        String result = reg.getString(group, key);
        assertEquals(expResult, result);
        System.out.print("PASS");
    }

    /**
     * Test of getStringArray method, of class RegistryImpl.
     */
    @Test
    public void testGetStringArray() {
        System.out.println("getStringArray");
        // Function not used.
    }

    /**
     * Test of getBoolean method, of class RegistryImpl.
     */
    @Test
    public void testGetBoolean() {
        System.out.println("getBoolean - case true");
        RegistryImpl reg = new RegistryImpl(new int[] {0,1,2});
        String group = "TEST";
        String key = "trueProperty";
        reg.setProperty(group, key, true);
        
        boolean result = reg.getBoolean(group, key);
        boolean expResult = true;
        assertEquals(expResult, result);
        System.out.println("PASS");
        
        System.out.println("getBoolean - case false");
        key = "falseProperty";
        reg.setProperty(group, key, false);
        
        result = reg.getBoolean(group, key);
        expResult = false;
        assertEquals(expResult, result);
        System.out.println("PASS"); 
        
        System.out.println("getBoolean - case non boolean");
        key = "nonBooleanProperty";
        reg.initGroup(group, new String[][]{{key, "5", "boolean"}});
        
        try{
            reg.getBoolean(group, key);
        } catch (Exception e) {       
            if (!(e instanceof RegistryPropertyException) 
                    || !(e.getMessage().startsWith("malform boolean value"))){
                fail("Wrong exception thrown");
            }
        }
        System.out.println("PASS");
    }

    /**
     * Test of getInteger method, of class RegistryImpl.
     */
    @Test
    public void testGetInteger() {
        System.out.println("getInteger - case valid int");
        RegistryImpl reg = new RegistryImpl(new int[] {0,1,2});
        String group = "TEST";
        String key = "intProperty";
        reg.initGroup(group, new String[][]{{key, "5", "int"}});
        
        int result = reg.getInteger(group, key);
        int expResult = 5;
        assertEquals(expResult, result);
        System.out.println("PASS");
        
        System.out.println("getInteger - case valid int");
        group = "TEST";
        key = "nonIntProperty";
        reg.initGroup(group, new String[][]{{key, "five", "int"}});
        
        try{
            reg.getInteger(group, key);
        }catch(Exception e){
            if (!(e instanceof NumberFormatException) || 
                    !(e.getMessage().startsWith("malformed int value"))){
                fail ("Wrong type of exception thrown");
            }
        }
        System.out.println("PASS");
    }

    /**
     * Test of getIntegerArray method, of class RegistryImpl.
     */
    @Test
    public void testGetIntegerArray() {
        System.out.println("getIntegerArray");
        //Function not used.
    }

    /**
     * Test of getLong method, of class RegistryImpl.
     */
    @Test
    public void testGetLong() {
        System.out.println("getLong");
        //Function not used.
    }

    /**
     * Test of getByte method, of class RegistryImpl.
     */
    @Test
    public void testGetByte() {
        System.out.println("getByte");
        //Function not used.
    }

    /**
     * Test of getByteArray method, of class RegistryImpl.
     */
    @Test
    public void testGetByteArray() {
        System.out.println("getByteArray");
        // Function not used/
    }

    /**
     * Test of getChar method, of class RegistryImpl.
     */
    @Test
    public void testGetChar() {
        System.out.println("getChar");
        // Function not used.
    }

    /**
     * Test of getCharArray method, of class RegistryImpl.
     */
    @Test
    public void testGetCharArray() {
        System.out.println("getCharArray");
        //Function not used
    }

    /**
     * Test of getDouble method, of class RegistryImpl.
     */
    @Test
    public void testGetDouble() {
        System.out.println("getDouble");
        //Function not used
    }

    /**
     * Test of getFloat method, of class RegistryImpl.
     */
    @Test
    public void testGetFloat() {
        System.out.println("getFloat");
        //function not used
    }

    /**
     * Test of getObject method, of class RegistryImpl.
     */
    @Test
    public void testGetObject() {
        System.out.println("getObject");
        // Function not used
    }

    /**
     * Test of getObjectArray method, of class RegistryImpl.
     */
    @Test
    public void testGetObjectArray() {
        System.out.println("getObjectArray");
        // Function not used
    }

    /**
     * Test of getShort method, of class RegistryImpl.
     */
    @Test
    public void testGetShort() {
        System.out.println("getShort");
        // Function not used
    }

    /**
     * Test of setProperty method, of class RegistryImpl.
     */
    @Test
    public void testSetProperty_3args_1() {
        System.out.println("setProperty");
        String group = "COLOUR";
        String key = "red";
        String value = "#FF0000";
        
        RegistryImpl instance = new RegistryImpl(new int[] { 0, 1, 2 });
        instance.setProperty(group, key, value);
        assertEquals(instance.getString(group, key), value);
    }

    /**
     * Test of setProperty method, of class RegistryImpl.
     */
    @Test
    public void testSetProperty_3args_2() {
        System.out.println("setProperty");
        String group = "ALPHABET";
        String key = "letters";
        String[] value = { "A", "B", "C" };
        
        RegistryImpl instance = new RegistryImpl(new int[] { 0, 1, 2 });
        try {
            instance.setProperty(group, key, value);
            fail("Array values are not supported");
        } catch (Exception e) {
            // Array values are not supported.
        }
    }

    /**
     * Test of setProperty method, of class RegistryImpl.
     */
    @Test
    public void testSetProperty_3args_3() {
        System.out.println("setProperty");
        String group = "TEST";
        String key = "prop";
        boolean value = false;
        
        RegistryImpl instance = new RegistryImpl(new int[] { 0, 1, 2 });
        instance.setProperty(group, key, value);
        assert(!instance.getBoolean(group, key));
    }

    /**
     * Test of setProperty method, of class RegistryImpl.
     */
    @Test
    public void testSetProperty_3args_4() {
        System.out.println("setProperty");
        String group = "NUMBERS";
        String key = "five";
        int value = 5;
        
        RegistryImpl instance = new RegistryImpl(new int[] { 0, 1, 2 });
        instance.setProperty(group, key, value);
        assertEquals(instance.getInteger(group, key), value);
    }

    /**
     * Test of setProperty method, of class RegistryImpl.
     */
    @Test
    public void testSetProperty_3args_5() {
        System.out.println("setProperty");
        
        String group = "NUMBERS";
        String key = "evens";
        int[] value = { 0, 2, 4 };
        
        RegistryImpl instance = new RegistryImpl(new int[] { 0, 1, 2 });
        try {
            instance.setProperty(group, key, value);
            fail("Array values are not supported");
        } catch (Exception e) {
            // Array values are not supported.
        }
    }

    /**
     * Test of setProperty method, of class RegistryImpl.
     */
    @Test
    public void testSetProperty_3args_6() {
        System.out.println("setProperty");
        String group = "LONG";
        String key = "alot";
        long value = 1234567890;
        
        RegistryImpl instance = new RegistryImpl(new int[] { 0, 1, 2 });
        instance.setProperty(group, key, value);
        assertEquals(instance.getLong(group, key), value);
    }

    /**
     * Test of setProperty method, of class RegistryImpl.
     */
    @Test
    public void testSetProperty_3args_7() {
        System.out.println("setProperty");
        String group = "NUMBER";
        String key = "byte";
        byte value = 127;
        
        RegistryImpl instance = new RegistryImpl(new int[] { 0, 1, 2 });
        instance.setProperty(group, key, value);
        assertEquals(instance.getByte(group, key), value);
    }

    /**
     * Test of setProperty method, of class RegistryImpl.
     */
    @Test
    public void testSetProperty_3args_8() {
        System.out.println("setProperty");
        String group = "NUMBERS";
        String key = "evens";
        byte[] value = { 0, 2, 4 };
        
        RegistryImpl instance = new RegistryImpl(new int[] { 0, 1, 2 });
        try {
            instance.setProperty(group, key, value);
            fail("Array values are not supported");
        } catch (Exception e) {
            // Array values are not supported.
        }
    }

    /**
     * Test of setProperty method, of class RegistryImpl.
     */
    @Test
    public void testSetProperty_3args_9() {
        System.out.println("setProperty");
        String group = "ALPHABET";
        String key = "letter";
        char value = 'a';
        
        RegistryImpl instance = new RegistryImpl(new int[] { 0, 1, 2 });
        instance.setProperty(group, key, value);
        assertEquals(instance.getChar(group, key), value);
    }

    /**
     * Test of setProperty method, of class RegistryImpl.
     */
    @Test
    public void testSetProperty_3args_10() {
        System.out.println("setProperty");
        String group = "ALPHABET";
        String key = "letters";
        char[] value = { 'a', 'b', 'c' };
        
        RegistryImpl instance = new RegistryImpl(new int[] { 0, 1, 2 });
        try {
            instance.setProperty(group, key, value);
            fail("Array values are not supported");
        } catch (Exception e) {
            // Array values are not supported.
        }
    }

    /**
     * Test of setProperty method, of class RegistryImpl.
     */
    @Test
    public void testSetProperty_3args_11() {
        System.out.println("setProperty");
        String group = "CONSTANTS";
        String key = "pi";
        double value = 3.14;
        
        RegistryImpl instance = new RegistryImpl(new int[] { 0, 1, 2 });
        instance.setProperty(group, key, value);
        assert(instance.getDouble(group, key) == value);
    }

    /**
     * Test of setProperty method, of class RegistryImpl.
     */
    @Test
    public void testSetProperty_3args_12() {
        System.out.println("setProperty");
        String group = "CONSTANTS";
        String key = "pi";
        float value = 3.14f;
        
        RegistryImpl instance = new RegistryImpl(new int[] { 0, 1, 2 });
        instance.setProperty(group, key, value);
        assert(instance.getFloat(group, key) == value);
    }

    /**
     * Test of setProperty method, of class RegistryImpl.
     */
    @Test
    public void testSetProperty_3args_13() {
        System.out.println("setProperty");
        // Not used.
    }

    /**
     * Test of setProperty method, of class RegistryImpl.
     */
    @Test
    public void testSetProperty_3args_14() {
        System.out.println("setProperty");
        // Not used.
    }

    /**
     * Test of setProperty method, of class RegistryImpl.
     */
    @Test
    public void testSetProperty_3args_15() {
        System.out.println("setProperty");
        String group = "NUMBERS";
        String key = "twenty";
        short value = 20;
        
        RegistryImpl instance = new RegistryImpl(new int[] { 0, 1, 2 });
        instance.setProperty(group, key, value);
        assert(instance.getShort(group, key) == value);
    }

    /**
     * Test of isProperty method, of class RegistryImpl.
     */
    @Test
    public void testIsProperty() {
        System.out.println("isProperty");
        
        String group = "CONSTANT";
        String key = "pi";
        double value = 3.14;
        
        RegistryImpl instance = new RegistryImpl(new int[] { 0, 1, 2 });
        instance.setProperty(group, key, value);
        assert(instance.isProperty(group, key));
        assert(!instance.isProperty("test", "notkey"));
    }

    /**
     * Test of isGroup method, of class RegistryImpl.
     */
    @Test
    public void testIsGroup() {
        System.out.println("isGroup");
        String group = "TEST";
        String non_group = "";
        String key;
        boolean result;
        
        RegistryImpl reg = new RegistryImpl(new int[] {0,1,2});
        key = "propertyTest";
        reg.initGroup(group, new String[][]{{key, "true", "boolean"}});
        
        result = reg.isGroup(group);
        assertTrue(result);
        result = reg.isGroup(non_group);
        assertFalse(result);
        
        System.out.println("PASS");
    }

    /**
     * Test of getGroups method, of class RegistryImpl.
     */
    @Test
    public void testGetGroups() {
        System.out.println("getGroups");
        
        Enumeration result;
        
        RegistryImpl reg = new RegistryImpl(new int[] {0,1,2});
        reg.initGroup("GROUP1", new String[][]{{"open.blank.default", "true", "boolean"}});
        reg.initGroup("GROUP2", new String[][]{{"max.file.history", "5", "int"}});
        reg.initGroup("GROUP3", new String[][]{{"show.file.toolbar", "true", "boolean"}});
        result = reg.getGroups();
        
        Vector groups = new Vector<String>();
        groups.add("GROUP1");
        groups.add("GROUP2");
        groups.add("GROUP3");
        
        while (result.hasMoreElements()) {
            assert(groups.contains(result.nextElement()));
        }
        
        System.out.println("PASS");
    }

    /*
     * Test of getKeys method, of class RegistryImpl.
     */
    @Test
    public void testGetKeys() {
        System.out.println("getKeys");
        String group = "TEST";
        Enumeration result;
        String[] keyStrings = {"key1", "key2", "key3"};
        
        RegistryImpl reg = new RegistryImpl(new int[] {0,1,2});
        Vector keys = new Vector();
        keys.add(keyStrings[0]);
        keys.add(keyStrings[1]);
        keys.add(keyStrings[2]);
        
        reg.initGroup(group, new String[][]{
                        {keyStrings[0], "true", "boolean"},
                        {keyStrings[1], "true", "boolean"},
                        {keyStrings[2], "true", "boolean"}
                });
        
        result = reg.getKeys(group);
        
        while (result.hasMoreElements()) {
            assert(keys.contains(result.nextElement()));
        }
        
        System.out.println("PASS");
    }

    /**
     * Test of sizeOf method, of class RegistryImpl.
     */
    @Test
    public void testSizeOf() {
        System.out.println("sizeOf");
        String group = "";
        
        // Check with a non-existent group.
        RegistryImpl instance = new RegistryImpl(new int[] {0,1,2});
        int expResult = 0;
        int result = instance.sizeOf(group);
        assertEquals(expResult, result);
        
        // Create a group with two properties.
        expResult = 2;
        group = "NUMBERS";
        instance.setProperty(group, "one", 1);
        instance.setProperty(group, "two", 2);
        result = instance.sizeOf(group);
        assertEquals(result, expResult);
    }

    /**
     * Test of isBlank method, of class RegistryImpl.
     */
    @Test
    public void testIsBlank() {
        System.out.println("isBlank");
        RegistryImpl instance = new RegistryImpl(new int[] { 0, 1, 2 });
        assert(instance.isBlank());
        
        String group = "NUMBERS";
        instance.setProperty(group, "one", 1);
        assert(!instance.isBlank());
        
    }

    /**
     * Test of size method, of class RegistryImpl.
     */
    @Test
    public void testSize() {
        System.out.println("size");
        RegistryImpl instance = new RegistryImpl(new int[] { 0, 1, 2 });
        assert(instance.size() == 0);
        
        String group = "NUMBERS";
        instance.setProperty(group, "one", 1);
        assert(instance.size() == 1);
    }

    /**
     * Test of deleteGroup method, of class RegistryImpl.
     */
    @Test
    public void testDeleteGroup() {
        System.out.println("deleteGroup");
        String group = "NUMBERS";
        String group2 = "ALPHABET";
        RegistryImpl instance = new RegistryImpl(new int[] { 0, 1, 2 });
        instance.setProperty(group, "one", 1);
        instance.setProperty(group2, "letter", 'a');
        
        // Try deleting a non-existent group.
        instance.deleteGroup("test");
        assert(instance.isGroup(group));
        assert(instance.isGroup(group2));
        
        // Delete alphabet group.
        instance.deleteGroup(group2);
        assert(instance.isGroup(group));
        assert(!instance.isGroup(group2));
    }

    /**
     * Test of deleteProperty method, of class RegistryImpl.
     */
    @Test
    public void testDeleteProperty() {
        System.out.println("deleteProperty");
        String group = "FRUIT";
        String apple = "apple";
        String orange = "orange";
        
        RegistryImpl instance = new RegistryImpl(new int[] { 0, 1, 2 });
        instance.setProperty(group, apple, true);
        instance.setProperty(group, orange, true);
        
        // Try removing a non-existent property.
        instance.deleteProperty(group, "banana");
        assert(instance.isProperty(group, apple));
        assert(instance.isProperty(group, orange));
        
        // Try removing orange.
        instance.deleteProperty(group, orange);
        assert(instance.isProperty(group, apple));
        assert(!instance.isProperty(group, orange));
    }

    /**
     * Test of deleteAll method, of class RegistryImpl.
     */
    @Test
    public void testDeleteAll() {
        System.out.println("deleteAll");
        String group = "FRUIT";
        String apple = "apple";
        String orange = "orange";
        
        RegistryImpl instance = new RegistryImpl(new int[] { 0, 1, 2 });
        instance.setProperty(group, apple, true);
        instance.setProperty(group, orange, true);
        instance.deleteAll();
        
        assert(instance.isBlank());
    }

    /**
     * Test of referenceGroup method, of class RegistryImpl.
     */
    @Test
    public void testReferenceGroup() {
        System.out.println("referenceGroup");
        String groupName = "Processor";
        String key = "type";
        String value = "x86";
        
        RegistryImpl instance = new RegistryImpl(new int[] { 0, 1, 2 });
        // Get reference of a non-existent group.
        assertNull(instance.referenceGroup("test"));
        
        // Add group, alter it, and see if the registry
        // was modified accordingly.
        instance.setProperty(groupName, key, value);
        RegistryGroup group = instance.referenceGroup(groupName);
        group.setProperty(key, "i386");
        
        assertEquals(instance.getString(groupName, key), "i386");
    }

    /**
     * Test of exportGroup method, of class RegistryImpl.
     */
    @Test
    public void testExportGroup() {
        System.out.println("exportGroup");
        String groupName = "Processor";
        String key = "type";
        String value = "x86";
        
        RegistryImpl instance = new RegistryImpl(new int[] { 0, 1, 2 });
        // Get copy of a non-existent group.
        assertNull(instance.exportGroup("test"));
        
        // Add group, alter it, and see if the registry
        // was not modified.
        instance.setProperty(groupName, key, value);
        RegistryGroup group = instance.exportGroup(groupName);
        group.setProperty(key, "i386");
        
        assertEquals(instance.getString(groupName, key), "x86");
    }

    /**
     * Test of importGroup method, of class RegistryImpl.
     */
    @Test
    public void testImportGroup() {
        System.out.println("importGroup");
        String groupName = "CLOCK";
        RegistryGroup group = new RegistryGroup();
        group.setProperty("time", "1030");
        
        RegistryImpl instance = new RegistryImpl(new int[] { 0, 1, 2 });
        instance.importGroup(groupName, group);
        
        // Insert a valid group.
        assertEquals(instance.referenceGroup(groupName), group);
        
        // Try to insert the same group twice.
        instance.importGroup(groupName, group);
        assert(instance.size() == 1);
        
        // Try passing in invalid arguments.
        try {
            instance.importGroup(groupName, null);
            instance.importGroup(null, group);
        } catch (IllegalArgumentException ex) {
            // Valid exception to be thrown.
        } catch (Exception e) {
            fail("Incorrect exception thrown.");
        }
    }

    /**
     * Test of initGroup method, of class RegistryImpl.
     */
    @Test
    public void testInitGroup() {
        System.out.println("initGroup");
        String group = "TEST";
        String[][] props = new String[][]{{"open.blank.default", "true", "boolean"}};
        
        RegistryImpl instance = new RegistryImpl(new int[] { 0, 1, 2 });
        instance.initGroup(group, props);
        
        assert(instance.isGroup(group));
        assert(instance.isProperty(group, "open.blank.default"));
        assert(instance.getBoolean(group, "open.blank.default"));
        
        // Try creating a group with null parameters.
        try {
            instance.initGroup(group, null);
            instance.initGroup(null, props);
        } catch (IllegalArgumentException e) {
            // Valid exception.
        } catch (Exception e) {
            fail("Incorrect exception thrown.");
        }
    }

    /**
     * Test of replaceGroup method, of class RegistryImpl.
     */
    @Test
    public void testReplaceGroup() {
        System.out.println("replaceGroup");
        String groupName = "Processor";
        String key = "type";
        String value = "x86";
        
        RegistryImpl instance = new RegistryImpl(new int[] { 0, 1, 2 });
        instance.setProperty(groupName, key, value);
        
        RegistryGroup group = new RegistryGroup();
        group.setProperty(key, "i386");
        
        // Replace valid group.
        instance.replaceGroup(groupName, group);
        assertEquals(instance.getString(groupName, key), "i386");
        
        // Try using invalid arguments.
        try {
            instance.replaceGroup(groupName, null);
            instance.replaceGroup(null, group);
        } catch (IllegalArgumentException e) {
            // Valid exception.
        } catch (Exception e) {
            fail("Incorrect exception thrown.");
        }
    }

    /**
     * Test of mergeRegistry method, of class RegistryImpl.
     */
    @Test
    public void testMergeRegistry() {
        System.out.println("mergeRegistry");
        RegistryImpl instance = new RegistryImpl(new int[] { 0, 1, 2 });
        try {
            instance.mergeRegistry(null);
        } catch (IllegalArgumentException e) {
            // Valid exception.
        } catch (Exception e) {
            fail("Incorrect exception thrown.");
        }
    }

    /**
     * Test of commit method, of class RegistryImpl.
     */
    @Test
    public void testCommit() throws Exception {
        System.out.println("commit");
        
        // Create a test registry with a group "TEST", which
        // contains the boolean property "testProp" set to true.
        RegistryImpl reg = new RegistryImpl(new int[] {0, 1, 2});
        File dataFile = new File("test\\com\\jmonkey\\export\\testWriter.txt");
        reg.initGroup("TEST", new String[][]{{"testProp", "true", "boolean"}});
        
        // Test without setting the data file.
        try {
            reg.commit();
        } catch (IOException e) {
            // Valid exception.
        } catch (Exception e) {
            fail("Incorrect exception thrown.");
        }
        
        // Set the data file.
        reg.setFile(dataFile);
        
        // Read back the created registry file and validate
        // its properties.
        reg.read(new FileReader(dataFile));
        assert(reg.size() == 1);
        assert(reg.isGroup("TEST"));
        assert(reg.isProperty("TEST", "testProp"));
        assert(reg.getBoolean("TEST", "testProp"));
    }

    /**
     * Test of revert method, of class RegistryImpl.
     */
    @Test
    public void testRevert() throws Exception {
        System.out.println("revert");
        
        // Create a test registry with a group "TEST", which
        // contains the boolean property "testProp" set to true.
        RegistryImpl reg = new RegistryImpl(new int[] {0, 1, 2});
        File dataFile = new File("test\\com\\jmonkey\\export\\testWriter.txt");
        reg.initGroup("TEST", new String[][]{{"testProp", "true", "boolean"}});
        reg.setFile(dataFile);
        reg.commit();
        
        // Alter the file.
        reg.setProperty("TEST", "prop2", 1);
        
        // Revert.
        reg.revert();
        
        assert(reg.size() == 1);
        assert(reg.isGroup("TEST"));
        assert(reg.isProperty("TEST", "testProp"));
        assert(reg.getBoolean("TEST", "testProp"));
    }
}
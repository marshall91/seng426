/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jmonkey.office.jwp;

import com.jmonkey.office.jwp.support.EditorActionManager;
import com.jmonkey.office.jwp.support.editors.TEXTEditor;
import com.jmonkey.export.RegistryFormatException;
import com.jmonkey.office.jwp.support.Editor;
import java.awt.Component;
import java.io.File;
import javax.swing.Action;
import javax.swing.JInternalFrame;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author tri
 */
public class DocumentManagerTest {
    
    private JWP app;
    private DocumentManager dm;
    private EditorActionManager eam;
    private String contentType = "test";
    
    public DocumentManagerTest() throws RegistryFormatException {
        app = new JWP(null);
        dm = new DocumentManager(app);
        eam = new EditorActionManager(app, dm);
    }


    /**
     * Test of editorNew method, of class DocumentManager.
     */
    @Test
    public void testEditorNew() {
        System.out.println("editorNew");
        DocumentManager instance = new DocumentManager(app);
        
        try{
            instance.editorNew();
        } catch(Exception ex){
            fail("Caught incorrect exception");
        }
    }

    /**
     * Test of editorOpen method, of class DocumentManager.
     */
    @Test
    public void testEditorOpen_File() {
        System.out.println("editorOpen");
        DocumentManager instance = new DocumentManager(app);
        File file = null;
        
        try{
            instance.editorOpen(file);
        } catch(Exception ex){
            System.out.println("Good Exception");
        }
    }

    /**
     * Test of editorOpen method, of class DocumentManager.
     */
    @Test
    public void testEditorOpen_0args() {
        System.out.println("editorOpen");
        DocumentManager instance = new DocumentManager(app);
        try{
            instance.editorOpen();
        } catch(Exception ex){
            fail("Bad Exception");
        }
    }

    /**
     * Test of editorOpenAs method, of class DocumentManager.
     */
    @Test
    public void testEditorOpenAs() {
        System.out.println("editorOpenAs");
        DocumentManager instance = new DocumentManager(app);
        try{
            instance.editorOpenAs();
        } catch(Exception ex){
            fail("Bad Exception");
        }
    }

    /**
     * Test of editorRevert method, of class DocumentManager.
     */
    @Test
    public void testEditorRevert() {
        System.out.println("editorRevert");
        Editor editor = null;
        DocumentManager instance = new DocumentManager(app);
        
        try{
            instance.editorRevert(editor);
        } catch(Exception ex){
            System.out.println("Good Exception");
        }
        
        editor = new TEXTEditor(eam);
        try{
            instance.editorRevert(editor);
        } catch(Exception ex){
            fail("Bad Exception");
        }
        
    }

    /**
     * Test of editorSave method, of class DocumentManager.
     */
    @Test
    public void testEditorSave() {
        System.out.println("editorSave");
        Editor editor = null;
        DocumentManager instance = new DocumentManager(app);
        
        try{
            instance.editorSave(editor);
        } catch(Exception ex){
            System.out.println("Good Exception");
        }
        
        editor = new TEXTEditor(eam);
        try{
            instance.editorSave(editor);
        } catch(Exception ex){
            fail("Bad Exception");
        }
    }

    /**
     * Test of editorSaveAs method, of class DocumentManager.
     */
    @Test
    public void testEditorSaveAs() {
        System.out.println("editorSaveAs");
        Editor editor = null;
        DocumentManager instance = new DocumentManager(app);
        
        try{
            instance.editorSaveAs(editor);
        } catch(Exception ex){
            System.out.println("Good Exception");
        }
        
        editor = new TEXTEditor(eam);
        try{
            instance.editorSaveAs(editor);
        } catch(Exception ex){
            fail("Bad Exception");
        }
    }

    /**
     * Test of editorSaveCopy method, of class DocumentManager.
     */
    @Test
    public void testEditorSaveCopy() {
        System.out.println("editorSaveCopy");
        Editor editor = null;
        DocumentManager instance = new DocumentManager(app);
        
        try{
            instance.editorSaveCopy(editor);
        } catch(Exception ex){
            System.out.println("Good Exception");
        }
        
        editor = new TEXTEditor(eam);
        try{
            instance.editorSaveCopy(editor);
        } catch(Exception ex){
            fail("Bad Exception");
        }
    }

    /**
     * Test of createDocumentFrame method, of class DocumentManager.
     */
    @Test
    public void testCreateDocumentFrame_3args() {
        System.out.println("createDocumentFrame");
        DocumentManager instance = new DocumentManager(app);
        File file = null;
        String title = "";
        String contentType = "";
        
        // Test with null file, empty title, content
        try{
            DocumentFrame result = instance.createDocumentFrame(file, title, contentType);
        } catch(Exception ex){
            System.out.println("Good Exception");
        }

        // Test with empty contenttype
        title = "testtitle";
        try{
            DocumentFrame result = instance.createDocumentFrame(file, title, contentType);
        } catch(Exception ex){
            System.out.println("Good Exception");
        }

        // Test with empty title
        title = "";
        contentType = "testtype";
        try{
            DocumentFrame result = instance.createDocumentFrame(file, title, contentType);
        } catch(Exception ex){
            System.out.println("Good Exception");
        }

        // Test with empty title
        title = "testtile";
        contentType = "testtype";
        try{
            DocumentFrame result = instance.createDocumentFrame(file, title, contentType);
        } catch(Exception ex){
            System.out.println("Good Exception");
        }
    
    }

    /**
     * Test of createDocumentFrame method, of class DocumentManager.
     */
    @Test
    public void testCreateDocumentFrame_File() {
        System.out.println("createDocumentFrame");
        File file = null;
        DocumentManager instance = new DocumentManager(app);
        // Test with null file
        try{
            DocumentFrame result = instance.createDocumentFrame(file);
        } catch(Exception ex){
            System.out.println("Good Exception");
        }
    }

    /**
     * Test of createDocumentFrame method, of class DocumentManager.
     */
    @Test
    public void testCreateDocumentFrame_String() {
        System.out.println("createDocumentFrame");
        String contentType = "";
        DocumentManager instance = new DocumentManager(app);
        // Test with null file, empty title, content
        try{
            DocumentFrame result = instance.createDocumentFrame(contentType);
        } catch(Exception ex){
            System.out.println("Good Exception");
        }
    }

    /**
     * Test of createDocumentFrame method, of class DocumentManager.
     */
    @Test
    public void testCreateDocumentFrame_0args() {
        System.out.println("createDocumentFrame");
        DocumentManager instance = new DocumentManager(app);
        DocumentFrame result = instance.createDocumentFrame();
        assertNotNull(result);
    }

    /**
     * Test of openDocumentList method, of class DocumentManager.
     */
    @Test
    public void testOpenDocumentList() {
        System.out.println("openDocumentList");
        DocumentManager instance = new DocumentManager(app);
        String[] result = instance.openDocumentList();
        assertNotNull(result);

    }

    /**
     * Test of getOpenDocument method, of class DocumentManager.
     */
    @Test
    public void testGetOpenDocument() {
        System.out.println("getOpenDocument");
        String name = "";
        DocumentManager instance = new DocumentManager(app);
        DocumentFrame result = instance.getOpenDocument(name);
        assertNull(result);
    }

    /**
     * Test of activateFrame method, of class DocumentManager.
     */
    @Test
    public void testActivateFrame() {
        System.out.println("activateFrame");
        JInternalFrame f = null;
        DocumentManager instance = new DocumentManager(app);

        try{
            instance.activateFrame(f);
        } catch(Exception ex){
            System.out.println("Good Exception");
        }
    }

    /**
     * Test of active method, of class DocumentManager.
     */
    @Test
    public void testActive() {
        System.out.println("active");
        DocumentManager instance = new DocumentManager(app);
        DocumentFrame result = instance.active();
        assertNull(result);
    }

    /**
     * Test of getApp method, of class DocumentManager.
     */
    @Test
    public void testGetApp() {
        System.out.println("getApp");
        DocumentManager instance = new DocumentManager(app);
        JWP result = instance.getApp();
        assertNotNull(result);

    }

    /**
     * Test of switchedDocument method, of class DocumentManager.
     */
    @Test
    public void testSwitchedDocument() {
        System.out.println("switchedDocument");
        DocumentFrame frame = null;
        boolean textSelected = false;
        DocumentManager instance = new DocumentManager(app);
        
        // Test with null frame, text selected false
        try{
            instance.switchedDocument(frame, textSelected);
        } catch(Exception ex){
            System.out.println("Good Exception");
        }

        // Test with null frame, text selected true
        textSelected = true;
        try{
            instance.switchedDocument(frame, textSelected);
        } catch(Exception ex){
            System.out.println("Good Exception");
        }
        
        // Test with new document frame
        frame = new DocumentFrame(app,contentType);
        try{
            instance.switchedDocument(frame, textSelected);
        } catch(Exception ex){
            fail("Bad Exception");
        }
    }

    /**
     * Test of cascade method, of class DocumentManager.
     */
    @Test
    public void testCascade() {
        System.out.println("cascade");
        DocumentFrame dframe = null;
        DocumentManager instance = new DocumentManager(app);
        
        // Test with null frame
        try{
            instance.cascade(dframe);
        } catch(Exception ex){
            System.out.println("Good Exception");
        }
        // Test with  new frame
        dframe = new DocumentFrame(app,contentType);
        try{
            instance.cascade(dframe);
        } catch(Exception ex){
            fail("Bad Exception");
        }
    }

    /**
     * Test of getCascadeAction method, of class DocumentManager.
     */
    @Test
    public void testGetCascadeAction() {
        System.out.println("getCascadeAction");
        DocumentManager instance = new DocumentManager(app);
        Action result = instance.getCascadeAction();
        assertNotNull(result);
    }

    /**
     * Test of cascadeAll method, of class DocumentManager.
     */
    @Test
    public void testCascadeAll() {
        System.out.println("cascadeAll");
        DocumentManager instance = new DocumentManager(app);
        
        try{
            instance.cascadeAll();
        } catch(Exception ex){
            fail("Bad Exception");
        }
    }

    /**
     * Test of getCloseAction method, of class DocumentManager.
     */
    @Test
    public void testGetCloseAction() {
        System.out.println("getCloseAction");
        DocumentManager instance = new DocumentManager(app);
        Action result = instance.getCloseAction();
        assertNotNull(result);
    }

    /**
     * Test of closeActiveDocument method, of class DocumentManager.
     */
    @Test
    public void testCloseActiveDocument() {
        System.out.println("closeActiveDocument");
        DocumentManager instance = app.getDesktopManager();
        Editor editor = EditorActionManager.getActiveEditor();
        editor.setChanged(true);
        try{
            instance.closeActiveDocument();
        } catch(Exception ex){
            fail("Bad Exception");
        }
    }

    /**
     * Test of getCloseAllAction method, of class DocumentManager.
     */
    @Test
    public void testGetCloseAllAction() {
        System.out.println("getCloseAllAction");
        DocumentManager instance = new DocumentManager(app);
        Action result = instance.getCloseAllAction();
        assertNotNull(result);
    }

    /**
     * Test of closeAllDocuments method, of class DocumentManager.
     */
    @Test
    public void testCloseAllDocuments() {
        System.out.println("closeAllDocuments");
        DocumentManager instance = new DocumentManager(app);
        instance.closeAllDocuments();
        Component[] comps = JWP.getDesktop().getComponents();
        assert(comps.length == 0);
    }

    /**
     * Test of getMinimizeAction method, of class DocumentManager.
     */
    @Test
    public void testGetMinimizeAction() {
        System.out.println("getMinimizeAction");
        DocumentManager instance = new DocumentManager(app);
        Action result = instance.getMinimizeAction();
        assertNotNull(result);
    }

    /**
     * Test of minimizeAll method, of class DocumentManager.
     */
    @Test
    public void testMinimizeAll() {
        System.out.println("minimizeAll");
        DocumentManager instance = new DocumentManager(app);
        
        try{
            instance.minimizeAll();
        } catch(Exception ex){
            fail("Bad Exception");
        }
    }

    /**
     * Test of getTileAction method, of class DocumentManager.
     */
    @Test
    public void testGetTileAction() {
        System.out.println("getTileAction");
        DocumentManager instance = new DocumentManager(app);
        Action result = instance.getTileAction();
        assertNotNull(result);
    }

    /**
     * Test of tileAll method, of class DocumentManager.
     */
    @Test
    public void testTileAll() {
        System.out.println("tileAll");
        DocumentManager instance = new DocumentManager(app);
        
        try{
            instance.tileAll();
        } catch(Exception ex){
            fail("Bad Exception");
        }
    }
}

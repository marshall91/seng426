/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jmonkey.office.jwp;

import com.jmonkey.export.RegistryFormatException;
import com.jmonkey.office.jwp.support.Editor;
import java.awt.event.FocusEvent;
import java.beans.PropertyChangeEvent;
import javax.swing.event.InternalFrameEvent;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author tri
 */
public class DocumentFrameTest {
    
    private JWP app;
    private String contentType = "test";
    
    public DocumentFrameTest() throws RegistryFormatException {
        app = new JWP(null);
    }


    /**
     * Test of getName method, of class DocumentFrame.
     */
    @Test
    public void testGetName() {
        System.out.println("getName");
        DocumentFrame instance = new DocumentFrame(app,contentType);
        try{
            String result = instance.getName();
            assertNotNull(result);
        } catch(NullPointerException e){
            System.out.println("Caught right exception");
        } catch(Exception e){
            fail("Caught incorrect exception");
        }
    }

    /**
     * Test of getEditor method, of class DocumentFrame.
     */
    @Test
    public void testGetEditor() {
        System.out.println("getEditor");
        DocumentFrame instance = new DocumentFrame(app,contentType);
        Editor result = instance.getEditor();
        assertNotNull(result);
    }

    /**
     * Test of vetoableChange method, of class DocumentFrame.
     */
    @Test
    public void testVetoableChange() throws Exception {
        System.out.println("vetoableChange");
        PropertyChangeEvent evt = null;
        DocumentFrame instance = new DocumentFrame(app,contentType);

        try{
            instance.vetoableChange(evt);
        } catch(NullPointerException e){
            System.out.println("Caught right exception");
        } catch(Exception e){
            fail("Caught incorrect exception");
        }
    }

    /**
     * Test of internalFrameOpened method, of class DocumentFrame.
     */
    @Test
    public void testInternalFrameOpened() {
        System.out.println("internalFrameOpened");
        InternalFrameEvent e = null;
        DocumentFrame instance = new DocumentFrame(app,contentType);

        try{
            instance.internalFrameOpened(e);
        } catch(NullPointerException ex){
            System.out.println("Caught right exception");
        } catch(Exception ex){
            fail("Caught incorrect exception");
        }
    }

    /**
     * Test of internalFrameClosing method, of class DocumentFrame.
     */
    @Test
    public void testInternalFrameClosing() {
        System.out.println("internalFrameClosing");
        InternalFrameEvent e = null;
        DocumentFrame instance = new DocumentFrame(app,contentType);

        try{
            instance.internalFrameClosing(e);
        } catch(NullPointerException ex){
            System.out.println("Caught right exception");
        } catch(Exception ex){
            fail("Caught incorrect exception");
        }
    }

    /**
     * Test of internalFrameClosed method, of class DocumentFrame.
     */
    @Test
    public void testInternalFrameClosed() {
        System.out.println("internalFrameClosed");
        InternalFrameEvent e = null;
        DocumentFrame instance = new DocumentFrame(app,contentType);
        try{
            instance.internalFrameClosed(e);
        } catch(NullPointerException ex){
            System.out.println("Caught right exception");
        } catch(Exception ex){
            fail("Caught incorrect exception");
        }
    }

    /**
     * Test of internalFrameIconified method, of class DocumentFrame.
     */
    @Test
    public void testInternalFrameIconified() {
        System.out.println("internalFrameIconified");
        InternalFrameEvent e = null;
        DocumentFrame instance = new DocumentFrame(app,contentType);

        try{
            instance.internalFrameIconified(e);
        } catch(NullPointerException ex){
            System.out.println("Caught right exception");
        } catch(Exception ex){
            fail("Caught incorrect exception");
        }
    }

    /**
     * Test of internalFrameDeiconified method, of class DocumentFrame.
     */
    @Test
    public void testInternalFrameDeiconified() {
        System.out.println("internalFrameDeiconified");
        InternalFrameEvent e = null;
        DocumentFrame instance = new DocumentFrame(app,contentType);
        
        try{
            instance.internalFrameDeiconified(e);
        } catch(NullPointerException ex){
            System.out.println("Caught right exception");
        } catch(Exception ex){
            fail("Caught incorrect exception");
        }
    }

    /**
     * Test of internalFrameActivated method, of class DocumentFrame.
     */
    @Test
    public void testInternalFrameActivated() {
        System.out.println("internalFrameActivated");
        InternalFrameEvent e = null;
        DocumentFrame instance = new DocumentFrame(app,contentType);
        
        try{
            instance.internalFrameActivated(e);
        } catch(NullPointerException ex){
            System.out.println("Caught right exception");
        } catch(Exception ex){
            fail("Caught incorrect exception");
        }
    }

    /**
     * Test of internalFrameDeactivated method, of class DocumentFrame.
     */
    @Test
    public void testInternalFrameDeactivated() {
        System.out.println("internalFrameDeactivated");
        InternalFrameEvent e = null;
        DocumentFrame instance = new DocumentFrame(app,contentType);
        
        try{
            instance.internalFrameDeactivated(e);
        } catch(NullPointerException ex){
            System.out.println("Caught right exception");
        } catch(Exception ex){
            fail("Caught incorrect exception");
        }
    }

    /**
     * Test of focusGained method, of class DocumentFrame.
     */
    @Test
    public void testFocusGained() {
        System.out.println("focusGained");
        FocusEvent e = null;
        DocumentFrame instance = new DocumentFrame(app,contentType);
        try{
            instance.focusGained(e);
        } catch(NullPointerException ex){
            System.out.println("Caught right exception");
        } catch(Exception ex){
            fail("Caught incorrect exception");
        }
    }

    /**
     * Test of focusLost method, of class DocumentFrame.
     */
    @Test
    public void testFocusLost() {
        System.out.println("focusLost");
        FocusEvent e = null;
        DocumentFrame instance = new DocumentFrame(app,contentType);

        try{
            instance.focusLost(e);
        } catch(NullPointerException ex){
            System.out.println("Caught right exception");
        } catch(Exception ex){
            fail("Caught incorrect exception");
        }
    }

    /**
     * Test of activate method, of class DocumentFrame.
     */
    @Test
    public void testActivate() {
        System.out.println("activate");
        DocumentFrame instance = new DocumentFrame(app,contentType);

        try{
            instance.activate();
        } catch(NullPointerException ex){
            System.out.println("Caught right exception");
        } catch(Exception ex){
            fail("Caught incorrect exception");
        }
    }
}

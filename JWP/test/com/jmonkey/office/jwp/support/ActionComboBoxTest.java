/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jmonkey.office.jwp.support;

import com.jmonkey.export.RegistryFormatException;
import com.jmonkey.office.jwp.DocumentManager;
import com.jmonkey.office.jwp.JWP;
import java.awt.Font;
import java.awt.ItemSelectable;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.lang.reflect.Field;
import java.util.Hashtable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.AbstractAction;
import javax.swing.Action;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Michael
 */
public class ActionComboBoxTest {
    private static EditorActionManager editorActionManager;
    private static Field actionsField;
    
    public ActionComboBoxTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        JWP app = null;
        try {
            app = new JWP(null);
        } catch (RegistryFormatException ex) {
            Logger.getLogger(ActionComboBoxTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        DocumentManager dm = new DocumentManager(app);
        editorActionManager = new EditorActionManager(app, dm);
        
        try 
        {
            actionsField = ActionComboBox.class.getDeclaredField("m_actions");
        } catch (Exception e)
        {
            System.out.println(e.getMessage());
        }
        
        if (actionsField == null) return;
        
        // Enable access to the private field for testing.
        actionsField.setAccessible(true);
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    
    /*
     * Test the constructors of class ActionComboBox.
     */
    @Test
    public void testConstructors() {
        System.out.println("constructors");
        
        // Empty constructor.
        ActionComboBox instance = new ActionComboBox();
        try {
            Hashtable m_actions = (Hashtable)actionsField.get(instance);
            assert(m_actions.isEmpty());
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        
        // Actions constructor.
        Action[] actions = new Action[2];
        actions[0] = editorActionManager.getBeepAction();
        actions[1] = editorActionManager.getUndoAction();
        
        try {
            instance = new ActionComboBox(actions);
            Hashtable m_actions = (Hashtable)actionsField.get(instance);
            assertEquals(m_actions.get((String)actions[0].getValue(Action.NAME)), actions[0]);
            assertEquals(m_actions.get((String)actions[1].getValue(Action.NAME)), actions[1]);
        } catch (Exception e) {
            fail(e.getMessage());
        }
        
        // Null constructor.
        try {
            instance = new ActionComboBox(null);
            Hashtable m_actions = (Hashtable)actionsField.get(instance);
            assert(m_actions.isEmpty());
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

    /**
     * Test of addItem method, of class ActionComboBox.
     */
    @Test
    public void testAddItem() {
        System.out.println("addItem");
        ActionComboBox instance = new ActionComboBox();
        
        // Insert valid instance of an action.
        Action action = editorActionManager.getBoldAction();
        instance.addItem(action);
        
        try {
            Hashtable m_actions = (Hashtable)actionsField.get(instance);
            assertEquals(action, m_actions.get((String)action.getValue(Action.NAME)));
        } catch (Exception e) {
            fail(e.getMessage());
        }
        
        // Try inserting the same action again, as above.
        instance.addItem(action);
        
        try {
            Hashtable m_actions = (Hashtable)actionsField.get(instance);
            assert(m_actions.size() == 1);
            assertEquals(action, m_actions.get((String)action.getValue(Action.NAME)));
        } catch (Exception e) {
            fail(e.getMessage());
        }
        
        // Insert null item.
        try {
            instance.addItem(null);
            Hashtable m_actions = (Hashtable)actionsField.get(instance);
            assert(m_actions.isEmpty());
        } catch(Exception e) {
            fail(e.getMessage());
        }
    }

    /**
     * Test of getItemAt method, of class ActionComboBox.
     */
    @Test
    public void testGetItemAt() {
        System.out.println("getItemAt");
        
        Action[] actions = new Action[3];
        actions[0] = editorActionManager.getItalicAction();
        actions[1] = editorActionManager.getUnderlineAction();
        actions[2] = editorActionManager.getCutAction();
        ActionComboBox instance = new ActionComboBox(actions);
        
        // Left boundary.
        int index = 0;
        Object result = instance.getItemAt(index);
        assertEquals(result, actions[0]);
        
        // In element.
        index = 1;
        result = instance.getItemAt(index);
        assertEquals(result, actions[1]);
        
        // Right boundary.
        index = 2;
        result = instance.getItemAt(index);
        assertEquals(result, actions[2]);
        
        // Out left boundary.
        index = -1;
        try {
            result = instance.getItemAt(index);
            assertEquals(result, null);
        } catch (Exception e) {
            fail(e.getMessage());
        }
        
        // Out right boundary.
        index = 3;
        try {
            result = instance.getItemAt(index);
            assertEquals(result, null);
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

    /**
     * Test of insertItemAt method, of class ActionComboBox.
     */
    @Test
    public void testInsertItemAt() {
        System.out.println("insertItemAt");
        
        Action[] actions = new Action[4];
        actions[0] = editorActionManager.getFontFaceAction(new Font("Arial", 0, 12));
        actions[1] = editorActionManager.getFontSizeAction(5);
        actions[2] = editorActionManager.getFontChooserAction();
        actions[3] = editorActionManager.getAlignLeftAction();
        ActionComboBox instance = new ActionComboBox(actions);
        Hashtable m_actions = new Hashtable();
        
        try {
            m_actions = (Hashtable)actionsField.get(instance);
        } catch (Exception e) {
            fail(e.getMessage());
        }
        
        // Insert at index 0 (boundary).
        Action action = editorActionManager.getCopyAction();
        instance.insertItemAt(action, 0);
        Object result = instance.getItemAt(0);
        assertEquals(result, action);
        
        // Insert at index 2 (in test).
        action = editorActionManager.getUndoAction();
        instance.insertItemAt(action, 2);
        result = instance.getItemAt(2);
        assertEquals(result, action);
        
        // Insert at index 5 (right boundary).
        action = editorActionManager.getItalicAction();
        instance.insertItemAt(action, 5);
        result = instance.getItemAt(5);
        assertEquals(result, action);
        
        // Try inserting two actions of the same type.
        instance.insertItemAt(action, 3);
        assert(m_actions.size() == 7);
        
        // Insert at index 8 (out right boundary).
        action = editorActionManager.getBoldAction();
        try {
            instance.insertItemAt(action, 8);
            assert(m_actions.size() == 7);
        } catch (Exception e) {
            fail(e.getMessage());
        }
        
        // Invalid index -1.
        try {
            instance.insertItemAt(action, -1);
            assert(m_actions.size() == 6);
        } catch (Exception e) {
            fail(e.getMessage());
        }
        
        // Try inserting null.
        try {
            instance.insertItemAt(null, 0);
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }
    
    private static final class TestAction extends AbstractAction {
        public boolean is_action_called;
        public TestAction() {
          super("test");
          is_action_called = false;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            is_action_called = true;
        }
  }

    /**
     * Test of itemStateChanged method, of class ActionComboBox.
     */
    @Test
    public void testItemStateChanged() {
        System.out.println("itemStateChanged");
        
        Action[] actions = new Action[1];
        TestAction action = new TestAction();
        actions[0] = action;
        
        ItemEvent event = new ItemEvent(new ItemSelectable() {
            @Override
            public Object[] getSelectedObjects() {
                return new Object[0];
            }

            @Override
            public void addItemListener(ItemListener l) {
            }

            @Override
            public void removeItemListener(ItemListener l) {
            }
        }, 0, "test", 0);
        
        // Invoke TestAction.
        ActionComboBox instance = new ActionComboBox(actions);
        instance.itemStateChanged(event);
        assert(action.is_action_called);
        
        action.is_action_called = false;
        event = new ItemEvent(new ItemSelectable() {
            @Override
            public Object[] getSelectedObjects() {
                return new Object[0];
            }

            @Override
            public void addItemListener(ItemListener l) {
            }

            @Override
            public void removeItemListener(ItemListener l) {
            }
        }, 0, "test2", 0);
        
        // Call an action that is not in the ActionComboBox.
        instance.itemStateChanged(event);
        assert(!action.is_action_called);
    }

    /**
     * Test of removeAllItems method, of class ActionComboBox.
     */
    @Test
    public void testRemoveAllItems() {
        System.out.println("removeAllItems");
        
        // Create an ActionComboBox with several actions.
        Action[] actions = new Action[3];
        actions[0] = editorActionManager.getFontFaceAction(new Font("Arial", 0, 12));
        actions[1] = editorActionManager.getFontSizeAction(5);
        actions[2] = editorActionManager.getFontChooserAction();
        ActionComboBox instance = new ActionComboBox(actions);
        instance.removeAllItems();
        
        Hashtable m_actions = null;
        try {
            m_actions = (Hashtable)actionsField.get(instance);
        } catch (Exception e) {
            fail(e.getMessage());
        }
        
        // Check if all of the actions have been removed.
        assert(m_actions.isEmpty());
    }

    /**
     * Test of removeItem method, of class ActionComboBox.
     */
    @Test
    public void testRemoveItem() {
        System.out.println("removeItem");
        Object anObject = null;
        ActionComboBox instance = new ActionComboBox();
        instance.removeItem(anObject);
    }

    /**
     * Test of removeItemAt method, of class ActionComboBox.
     */
    @Test
    public void testRemoveItemAt() {
        System.out.println("removeItemAt");
        int anIndex = 0;
        ActionComboBox instance = new ActionComboBox();
        instance.removeItemAt(anIndex);
    }
}
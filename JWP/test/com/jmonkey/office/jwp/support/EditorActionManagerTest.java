/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jmonkey.office.jwp.support;

import com.jmonkey.export.RegistryFormatException;
import com.jmonkey.office.jwp.DocumentManager;
import com.jmonkey.office.jwp.JWP;
import com.jmonkey.office.jwp.support.editors.StyledEditor;
import com.jmonkey.office.jwp.support.editors.TEXTEditor;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.lang.reflect.Field;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.SwingUtilities;
import javax.swing.text.AbstractDocument;
import javax.swing.text.AttributeSet;
import javax.swing.text.Document;
import javax.swing.text.Element;
import javax.swing.text.JTextComponent;
import javax.swing.text.MutableAttributeSet;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Michael
 */
public class EditorActionManagerTest {
    
    private EditorActionManager instance;
    private Field editorField;
    private Field actionsField;
    private JWP app;
    
    public EditorActionManagerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        try {
            app = new JWP(null);
        } catch (RegistryFormatException ex) {
            Logger.getLogger(ActionComboBoxTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        DocumentManager dm = new DocumentManager(app);
        instance = new EditorActionManager(app, dm);
        
        // Get access to the editor field.
        try 
        {
            editorField = EditorActionManager.class.getDeclaredField("s_editor");
            actionsField = EditorActionManager.class.getDeclaredField("m_actions");
        } catch (Exception e)
        {
            System.out.println(e.getMessage());
        }
        
        if (editorField == null) return;
        if (actionsField == null) return;
        
        // Enable access to the private field for testing.
        editorField.setAccessible(true);
        actionsField.setAccessible(true);
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of activate method, of class EditorActionManager.
     */
    @Test
    public void testActivate() {
        System.out.println("activate");
        
        Editor editor = new StyledEditor(instance) {
            @Override
            public String[] getFileExtensions() {
                return null;
            }
            
            @Override
            public String getContentType() {
                return "text/rtf";
            }
        };
        
        // Set a new editor.
        instance.activate(editor);
        try {
            Editor s_editor = (Editor) editorField.get(instance);
            assert(editor.equals(s_editor));
        } catch (Exception e) {
            fail(e.getMessage());
        }
        
        // Check with null parameter.
        try {
            instance.activate(null);
        } catch (Exception e) {
            fail(e.getMessage());
        }
        
        assert(EditorActionManager.isActiveEditor());
    }

    /**
     * Test of createDefaultColourActions method, of class EditorActionManager.
     */
    @Test
    public void testCreateDefaultColourActions() {
        System.out.println("createDefaultColourActions");
        Action[] expResult = new Action[14];
        expResult[0] = instance.getColourAction("White", Color.white);
        expResult[1] = instance.getColourAction("Black", Color.black);
        expResult[2] = instance.getColourAction("Red", Color.red);
        expResult[3] = instance.getColourAction("Green", Color.green);
        expResult[4] = instance.getColourAction("Blue", Color.blue);
        expResult[5] = instance.getColourAction("Orange", Color.orange);
        expResult[6] = instance.getColourAction("Dark Gray", Color.darkGray);
        expResult[7] = instance.getColourAction("Gray", Color.gray);
        expResult[8] = instance.getColourAction("Light Gray", Color.lightGray);
        expResult[9] = instance.getColourAction("Cyan", Color.cyan);
        expResult[10] = instance.getColourAction("Magenta", Color.magenta);
        expResult[11] = instance.getColourAction("Pink", Color.pink);
        expResult[12] = instance.getColourAction("Yellow", Color.yellow);
        
        Action[] result = instance.createDefaultColourActions();
        assertArrayEquals(expResult, result);
    }

    /**
     * Test of createDefaultFontFaceActions method, of class EditorActionManager.
     */
    @Test
    public void testCreateDefaultFontFaceActions() {
        System.out.println("createDefaultFontFaceActions");
        
        // Get the default font face actions.
        Action[] result = instance.createDefaultFontFaceActions();
        assertNotNull(result);
        assert(result.length != 0);
        
        // Validate that they are all instances of the FontFamilyAction class.
        String fontFamilyActionClass = "com.jmonkey.office.jwp.support.EditorActionManager$FontFamilyAction";
        for (Action action : result) {
            assertNotNull(action);
            assert(action.getClass().getName().equals(fontFamilyActionClass));
        }
    }

    /**
     * Test of createInputAttributes method, of class EditorActionManager.
     */
    @Test
    public void testCreateInputAttributes() {
        System.out.println("createInputAttributes");
        
        Editor editor = EditorActionManager.getActiveEditor();
        JTextComponent textComp = (JTextComponent) editor.getTextComponent();
        StyledDocument doc = (StyledDocument) textComp.getDocument();
        Element element = doc.getCharacterElement(0);
        
        MutableAttributeSet set = new SimpleAttributeSet();
        StyleConstants.setBackground(set, Color.red);
        StyleConstants.setComponent(set, new JButton());
        StyleConstants.setIcon(set, new Icon() {

            @Override
            public void paintIcon(Component c, Graphics g, int x, int y) {
            }

            @Override
            public int getIconWidth() {
                return 0;
            }

            @Override
            public int getIconHeight() {
                return 0;
            }
        });
        
        doc.setCharacterAttributes(0, 1, set, true);
        instance.createInputAttributes(element, set);
        
        // Verify that only valid attributes are set.
        assertNull(set.getAttribute(StyleConstants.ComponentAttribute));
        assertNull(set.getAttribute(StyleConstants.IconAttribute));
        assertNull(set.getAttribute(AbstractDocument.ElementNameAttribute));
        assertNull(set.getAttribute(StyleConstants.ComposedTextAttribute));
        assertEquals(set.getAttribute(StyleConstants.Background), Color.red);
        
        // Check null parameters.
        try {
            instance.createInputAttributes(element, null);
        } catch (Exception e) {
            fail(e.getMessage());
        }
        
        try {
            instance.createInputAttributes(null, set);
            fail("Exception should have been thrown.");
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

    /**
     * Test of deactivate method, of class EditorActionManager.
     */
    @Test
    public void testDeactivate() {
        System.out.println("deactivate");
        
        // Deactivate current editor.
        Editor editor = EditorActionManager.getActiveEditor();
        instance.deactivate(editor);
        assert(!EditorActionManager.isActiveEditor());
        
        instance.activate(editor);
        
        // Try with null parameters.
        try {
            instance.deactivate(null);
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }
    
    public Map getActionsField() {
        Map m_actions = null;
        
        try {
            m_actions = (Map) actionsField.get(instance);
        } catch(Exception e) {
            fail(e.getMessage());
        }
        
        if (m_actions == null) {
            fail("No actions stored within the EditorActionManager");
            return null;
        }
        
        return m_actions;
    }
    
    public Editor getEditorField() {
        Editor s_editor = null;
        
        try {
            s_editor = (Editor) editorField.get(instance);
        } catch(Exception e) {
            fail(e.getMessage());
        }
        
        if (s_editor == null) {
            fail("No editor stored within the EditorActionManager");
            return null;
        }
        
        return s_editor;
    }

    /**
     * Test of enableAction method, of class EditorActionManager.
     */
    @Test
    public void testEnableAction() {
        System.out.println("enableAction");
        
        // Invalid pattern check.
        instance.enableAction("hello", false);
        
        Map m_actions = getActionsField();
        
        // Check to make sure that all of the actions are still enabled.
        for (Object action : m_actions.values()) {
            assert(((Action)action).isEnabled());
        }
        
        // Check for a specific action (Bold) to be enabled.
        instance.enableAction(EditorActionManager.BD_A_P, false);
        assert(!instance.getActionByKey(EditorActionManager.BD_A_P).isEnabled());
        
        // Test null parameter.
        try {
            instance.enableAction(null, false);
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

    /**
     * Test of enableColourActions method, of class EditorActionManager.
     */
    @Test
    public void testEnableColourActions() {
        System.out.println("enableColourActions");
        Action colourChooser = instance.getColourChooserAction();
        
        Map m_actions = getActionsField();
        
        // Test enabling colour actions.
        instance.enableColourActions(true);
        
        String foregroundActionClass = "com.jmonkey.office.jwp.support.EditorActionManager$ForegroundAction";
        for (Object action : m_actions.values()) {
            if (action.getClass().getName().equals(foregroundActionClass) ||
                action.getClass().getName().equals(colourChooser.getClass().getName())) {
                assert(((Action)action).isEnabled());
            }
        }
        
        // Test disabling colour actions.
        instance.enableColourActions(false);
        
        for (Object action : m_actions.values()) {
            if (action.getClass().getName().equals(foregroundActionClass) ||
                action.getClass().getName().equals(colourChooser.getClass().getName())) {
                assert(!((Action)action).isEnabled());
            }
        }
    }

    /**
     * Test of enableDocumentActions method, of class EditorActionManager.
     */
    @Test
    public void testEnableDocumentActions() {
        System.out.println("enableDocumentActions");
        
        // Test enabling the document actions.
        instance.enableDocumentActions(true);
        assert(instance.getActionByKey(EditorActionManager.C_A_P).isEnabled());
        assert(instance.getActionByKey(EditorActionManager.CO_A_P).isEnabled());
        assert(instance.getActionByKey(EditorActionManager.P_A_P).isEnabled());
        assert(instance.getActionByKey(EditorActionManager.SEL_A_P).isEnabled());
        assert(instance.getActionByKey(EditorActionManager.SEL_N_A_P).isEnabled());
        assert(instance.getActionByKey(EditorActionManager.UDO_A_AP).isEnabled());
        assert(instance.getActionByKey(EditorActionManager.RDO_A_P).isEnabled());
        assert(instance.getActionByKey(EditorActionManager.SEA_A_P).isEnabled());
        assert(instance.getActionByKey(EditorActionManager.REP_A_P).isEnabled());
        
        // Test disabling the document actions.
        instance.enableDocumentActions(false);
        assert(!instance.getActionByKey(EditorActionManager.C_A_P).isEnabled());
        assert(!instance.getActionByKey(EditorActionManager.CO_A_P).isEnabled());
        assert(!instance.getActionByKey(EditorActionManager.P_A_P).isEnabled());
        assert(!instance.getActionByKey(EditorActionManager.SEL_A_P).isEnabled());
        assert(!instance.getActionByKey(EditorActionManager.SEL_N_A_P).isEnabled());
        assert(!instance.getActionByKey(EditorActionManager.UDO_A_AP).isEnabled());
        assert(!instance.getActionByKey(EditorActionManager.RDO_A_P).isEnabled());
        assert(!instance.getActionByKey(EditorActionManager.SEA_A_P).isEnabled());
        assert(!instance.getActionByKey(EditorActionManager.REP_A_P).isEnabled());
    }

    /**
     * Test of enableFontActions method, of class EditorActionManager.
     */
    @Test
    public void testEnableFontActions() {
        System.out.println("enableFontActions");
        
        Action fontSizeAction = instance.getFontSizeAction(5);
        Action fontFamilyAction = instance.getFontFaceAction("Arial");
        Action fontChooserAction = instance.getFontChooserAction();
        
        Map m_actions = getActionsField();
        
        // Test enabling font actions.
        instance.enableFontActions(true);
        
        for (Object action : m_actions.values()) {
            if (action.getClass().getName().equals(fontSizeAction.getClass().getName()) ||
                action.getClass().getName().equals(fontFamilyAction.getClass().getName()) ||
                action.getClass().getName().equals(fontChooserAction.getClass().getName())) {
                assert(((Action)action).isEnabled());
            }
        }
        
        // Test disabling font actions.
        instance.enableFontActions(false);
        
        for (Object action : m_actions.values()) {
            if (action.getClass().getName().equals(fontSizeAction.getClass().getName()) ||
                action.getClass().getName().equals(fontFamilyAction.getClass().getName()) ||
                action.getClass().getName().equals(fontChooserAction.getClass().getName())) {
                assert(!((Action)action).isEnabled());
            }
        }
    }

    /**
     * Test of enableFormatActions method, of class EditorActionManager.
     */
    @Test
    public void testEnableFormatActions() {
        System.out.println("enableFormatActions");
        
        // Test enabling the format actions.
        instance.enableFormatActions(true);
        assert(instance.getActionByKey(EditorActionManager.A_L_A_P).isEnabled());
        assert(instance.getActionByKey(EditorActionManager.A_R_A_P).isEnabled());
        assert(instance.getActionByKey(EditorActionManager.A_C_A_P).isEnabled());
        assert(instance.getActionByKey(EditorActionManager.A_J_A_P).isEnabled());
        assert(instance.getActionByKey(EditorActionManager.BD_A_P).isEnabled());
        assert(instance.getActionByKey(EditorActionManager.IT_A_P).isEnabled());
        assert(instance.getActionByKey(EditorActionManager.U_A_P).isEnabled());
        assert(instance.getActionByKey(EditorActionManager.STR_A_P).isEnabled());
        assert(instance.getActionByKey(EditorActionManager.C_C_A_P).isEnabled());
        assert(instance.getActionByKey(EditorActionManager.F_C_A_P).isEnabled());
        
        // Test disabling the format actions.
        instance.enableFormatActions(false);
        assert(!instance.getActionByKey(EditorActionManager.A_L_A_P).isEnabled());
        assert(!instance.getActionByKey(EditorActionManager.A_R_A_P).isEnabled());
        assert(!instance.getActionByKey(EditorActionManager.A_C_A_P).isEnabled());
        assert(!instance.getActionByKey(EditorActionManager.A_J_A_P).isEnabled());
        assert(!instance.getActionByKey(EditorActionManager.BD_A_P).isEnabled());
        assert(!instance.getActionByKey(EditorActionManager.IT_A_P).isEnabled());
        assert(!instance.getActionByKey(EditorActionManager.U_A_P).isEnabled());
        assert(!instance.getActionByKey(EditorActionManager.STR_A_P).isEnabled());
        assert(!instance.getActionByKey(EditorActionManager.C_C_A_P).isEnabled());
        assert(!instance.getActionByKey(EditorActionManager.F_C_A_P).isEnabled());
    }

    /**
     * Test of enableGenericActions method, of class EditorActionManager.
     */
    @Test
    public void testEnableGenericActions() {
        System.out.println("enableGenericActions");
        
        // Test enabling generic actions.
        instance.enableGenericActions(true);
        assert(instance.getActionByKey(EditorActionManager.B_A_P).isEnabled());
        
        // Test disabling generic actions.
        instance.enableGenericActions(false);
        assert(!instance.getActionByKey(EditorActionManager.B_A_P).isEnabled());
    }

    /**
     * Test of getActionByKey method, of class EditorActionManager.
     */
    @Test
    public void testGetActionByKey() {
        System.out.println("getActionByKey");
        
        // Get the bold action by key.
        Action boldAction = instance.getActionByKey(EditorActionManager.BD_A_P);
        String boldClassName = "com.jmonkey.office.jwp.support.EditorActionManager$BoldAction";
        assertEquals(boldAction.getClass().getName(), boldClassName);
        
        // Check for an invalid key.
        Action action = instance.getActionByKey("test");
        assertNull(action);
        
        // Try null parameter.
        try {
            assertNull(instance.getActionByKey(null));
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

    /**
     * Test of getActiveEditor method, of class EditorActionManager.
     */
    @Test
    public void testGetActiveEditor() {
        System.out.println("getActiveEditor");
        
        Editor editorA = new StyledEditor(instance) {
            @Override
            public String[] getFileExtensions() {
                return null;
            }
            
            @Override
            public String getContentType() {
                return "text/rtf";
            }
        };
        
        instance.activate(editorA);
        assertEquals(EditorActionManager.getActiveEditor(), editorA);
        
        Editor editorB = new TEXTEditor(instance);
        instance.activate(editorB);
        assertEquals(EditorActionManager.getActiveEditor(), editorB);
    }

    /**
     * Test of getAlignCenterAction method, of class EditorActionManager.
     */
    @Test
    public void testGetAlignCenterAction() {
        System.out.println("getAlignCenterAction");
        
        String alignmentClassName = "com.jmonkey.office.jwp.support.EditorActionManager$AlignmentAction";
        Action action = instance.getAlignCenterAction();
        assertEquals(action.getClass().getName(), alignmentClassName);
    }

    /**
     * Test of getAlignJustifyAction method, of class EditorActionManager.
     */
    @Test
    public void testGetAlignJustifyAction() {
        System.out.println("getAlignJustifyAction");
        
        String alignmentClassName = "com.jmonkey.office.jwp.support.EditorActionManager$AlignmentAction";
        Action action = instance.getAlignJustifyAction();
        assertEquals(action.getClass().getName(), alignmentClassName);
    }

    /**
     * Test of getAlignLeftAction method, of class EditorActionManager.
     */
    @Test
    public void testGetAlignLeftAction() {
        System.out.println("getAlignLeftAction");
        
        String alignmentClassName = "com.jmonkey.office.jwp.support.EditorActionManager$AlignmentAction";
        Action action = instance.getAlignLeftAction();
        assertEquals(action.getClass().getName(), alignmentClassName);
    }

    /**
     * Test of getAlignRightAction method, of class EditorActionManager.
     */
    @Test
    public void testGetAlignRightAction() {
        System.out.println("getAlignRightAction");
        
        String alignmentClassName = "com.jmonkey.office.jwp.support.EditorActionManager$AlignmentAction";
        Action action = instance.getAlignRightAction();
        assertEquals(action.getClass().getName(), alignmentClassName);
    }

    /**
     * Test of getBeepAction method, of class EditorActionManager.
     */
    @Test
    public void testGetBeepAction() {
        System.out.println("getBeepAction");
        
        String beepClassName = "com.jmonkey.office.jwp.support.EditorActionManager$BeepAction";
        Action action = instance.getBeepAction();
        assertEquals(action.getClass().getName(), beepClassName);
    }

    /**
     * Test of getBoldAction method, of class EditorActionManager.
     */
    @Test
    public void testGetBoldAction() {
        System.out.println("getBoldAction");
        
        String boldClassName = "com.jmonkey.office.jwp.support.EditorActionManager$BoldAction";
        Action action = instance.getBoldAction();
        assertEquals(action.getClass().getName(), boldClassName);
    }

    /**
     * Test of getColourAction method, of class EditorActionManager.
     */
    @Test
    public void testGetColourAction() {
        System.out.println("getColourAction");
        
        String colourClassName = "com.jmonkey.office.jwp.support.EditorActionManager$ForegroundAction";
        Action action = instance.getColourAction("Test", Color.red);
        assertEquals(action.getClass().getName(), colourClassName);
    }

    /**
     * Test of getColourAtCaret method, of class EditorActionManager.
     */
    @Test
    public void testGetColourAtCaret() {
        System.out.println("getColourAtCaret");
        
        Editor editor = EditorActionManager.getActiveEditor();
        assertNotNull(instance.getColourAtCaret());
        
        MutableAttributeSet attr = new SimpleAttributeSet();
        StyleConstants.setForeground(attr, Color.red);
        instance.setCharacterAttributes(editor.getTextComponent(), attr, true);
        assert(instance.getColourAtCaret() == Color.red);
        
        try {
            editorField.set(instance, null);
        } catch (Exception e) {
            fail(e.getMessage());
        }
        
        assertNull(instance.getColourAtCaret());
    }

    /**
     * Test of getColourChooserAction method, of class EditorActionManager.
     */
    @Test
    public void testGetColourChooserAction() {
        System.out.println("getColourChooserAction");
       
        String colourChooserClassName = "com.jmonkey.office.jwp.support.EditorActionManager$ColourChooserAction";
        Action action = instance.getColourChooserAction();
        assertEquals(action.getClass().getName(), colourChooserClassName);
    }

    /**
     * Test of getCopyAction method, of class EditorActionManager.
     */
    @Test
    public void testGetCopyAction() {
        System.out.println("getCopyAction");
        
        String className = "com.jmonkey.office.jwp.support.EditorActionManager$CopyAction";
        Action action = instance.getCopyAction();
        assertEquals(action.getClass().getName(), className);
    }

    /**
     * Test of getCutAction method, of class EditorActionManager.
     */
    @Test
    public void testGetCutAction() {
        System.out.println("getCutAction");
        
        String className = "com.jmonkey.office.jwp.support.EditorActionManager$CutAction";
        Action action = instance.getCutAction();
        assertEquals(action.getClass().getName(), className);
    }

    /**
     * Test of getFontChooserAction method, of class EditorActionManager.
     */
    @Test
    public void testGetFontChooserAction() {
        System.out.println("getFontChooserAction");
        
        String className = "com.jmonkey.office.jwp.support.EditorActionManager$FontChooserAction";
        Action action = instance.getFontChooserAction();
        assertEquals(action.getClass().getName(), className);
    }

    /**
     * Test of getFontFaceAction method, of class EditorActionManager.
     */
    @Test
    public void testGetFontFaceAction_Font() {
        System.out.println("getFontFaceAction");
        
        String className = "com.jmonkey.office.jwp.support.EditorActionManager$FontFamilyAction";
        Action action = instance.getFontFaceAction(new Font("Arial", Font.BOLD, 12));
        assertEquals(action.getClass().getName(), className);
    }

    /**
     * Test of getFontFaceAction method, of class EditorActionManager.
     */
    @Test
    public void testGetFontFaceAction_String() {
        System.out.println("getFontFaceAction");
        
        String className = "com.jmonkey.office.jwp.support.EditorActionManager$FontFamilyAction";
        Action action = instance.getFontFaceAction("Arial");
        assertEquals(action.getClass().getName(), className);
    }

    /**
     * Test of getFontSizeAction method, of class EditorActionManager.
     */
    @Test
    public void testGetFontSizeAction() {
        System.out.println("getFontSizeAction");
        
        String className = "com.jmonkey.office.jwp.support.EditorActionManager$FontSizeAction";
        Action action = instance.getFontSizeAction(5);
        assertEquals(action.getClass().getName(), className);
    }

    /**
     * Test of getItalicAction method, of class EditorActionManager.
     */
    @Test
    public void testGetItalicAction() {
        System.out.println("getItalicAction");
        
        String className = "com.jmonkey.office.jwp.support.EditorActionManager$ItalicAction";
        Action action = instance.getItalicAction();
        assertEquals(action.getClass().getName(), className);
    }

    /**
     * Test of getNewAction method, of class EditorActionManager.
     */
    @Test
    public void testGetNewAction() {
        System.out.println("getNewAction");
        
        String className = "com.jmonkey.office.jwp.support.EditorActionManager$NewAction";
        Action action = instance.getNewAction();
        assertEquals(action.getClass().getName(), className);
    }

    /**
     * Test of getOpenAction method, of class EditorActionManager.
     */
    @Test
    public void testGetOpenAction() {
        System.out.println("getOpenAction");
        
        String className = "com.jmonkey.office.jwp.support.EditorActionManager$OpenAction";
        Action action = instance.getOpenAction();
        assertEquals(action.getClass().getName(), className);
    }

    /**
     * Test of getOpenAsAction method, of class EditorActionManager.
     */
    @Test
    public void testGetOpenAsAction() {
        System.out.println("getOpenAsAction");
        
        String className = "com.jmonkey.office.jwp.support.EditorActionManager$OpenAsAction";
        Action action = instance.getOpenAsAction();
        assertEquals(action.getClass().getName(), className);
    }

    /**
     * Test of getPasteAction method, of class EditorActionManager.
     */
    @Test
    public void testGetPasteAction() {
        System.out.println("getPasteAction");
        
        String className = "com.jmonkey.office.jwp.support.EditorActionManager$PasteAction";
        Action action = instance.getPasteAction();
        assertEquals(action.getClass().getName(), className);
    }

    /**
     * Test of getRedoAction method, of class EditorActionManager.
     */
    @Test
    public void testGetRedoAction() {
        System.out.println("getRedoAction");
        
        String className = "com.jmonkey.office.jwp.support.EditorActionManager$RedoAction";
        Action action = instance.getRedoAction();
        assertEquals(action.getClass().getName(), className);
    }

    /**
     * Test of getReplaceAction method, of class EditorActionManager.
     */
    @Test
    public void testGetReplaceAction() {
        System.out.println("getReplaceAction");
        
        String className = "com.jmonkey.office.jwp.support.EditorActionManager$ReplaceAction";
        Action action = instance.getReplaceAction();
        assertEquals(action.getClass().getName(), className);
    }

    /**
     * Test of getRevertAction method, of class EditorActionManager.
     */
    @Test
    public void testGetRevertAction() {
        System.out.println("getRevertAction");
        
        String className = "com.jmonkey.office.jwp.support.EditorActionManager$RevertAction";
        Action action = instance.getRevertAction();
        assertEquals(action.getClass().getName(), className);
    }

    /**
     * Test of getSaveAction method, of class EditorActionManager.
     */
    @Test
    public void testGetSaveAction() {
        System.out.println("getSaveAction");
        
        String className = "com.jmonkey.office.jwp.support.EditorActionManager$SaveAction";
        Action action = instance.getSaveAction();
        assertEquals(action.getClass().getName(), className);
    }

    /**
     * Test of getSaveAsAction method, of class EditorActionManager.
     */
    @Test
    public void testGetSaveAsAction() {
        System.out.println("getSaveAsAction");
        
        String className = "com.jmonkey.office.jwp.support.EditorActionManager$SaveAsAction";
        Action action = instance.getSaveAsAction();
        assertEquals(action.getClass().getName(), className);
    }

    /**
     * Test of getSaveCopyAction method, of class EditorActionManager.
     */
    @Test
    public void testGetSaveCopyAction() {
        System.out.println("getSaveCopyAction");
        
        String className = "com.jmonkey.office.jwp.support.EditorActionManager$SaveCopyAction";
        Action action = instance.getSaveCopyAction();
        assertEquals(action.getClass().getName(), className);
    }

    /**
     * Test of getSearchAction method, of class EditorActionManager.
     */
    @Test
    public void testGetSearchAction() {
        System.out.println("getSearchAction");
        
        String className = "com.jmonkey.office.jwp.support.EditorActionManager$SearchAction";
        Action action = instance.getSearchAction();
        assertEquals(action.getClass().getName(), className);
    }

    /**
     * Test of getSelectAllAction method, of class EditorActionManager.
     */
    @Test
    public void testGetSelectAllAction() {
        System.out.println("getSelectAllAction");
        
        String className = "com.jmonkey.office.jwp.support.EditorActionManager$SelectAllAction";
        Action action = instance.getSelectAllAction();
        assertEquals(action.getClass().getName(), className);
    }

    /**
     * Test of getSelectNoneAction method, of class EditorActionManager.
     */
    @Test
    public void testGetSelectNoneAction() {
        System.out.println("getSelectNoneAction");
        
        String className = "com.jmonkey.office.jwp.support.EditorActionManager$SelectNoneAction";
        Action action = instance.getSelectNoneAction();
        assertEquals(action.getClass().getName(), className);
    }

    /**
     * Test of getStrikeThroughAction method, of class EditorActionManager.
     */
    @Test
    public void testGetStrikeThroughAction() {
        System.out.println("getStrikeThroughAction");
        
        String className = "com.jmonkey.office.jwp.support.EditorActionManager$StrikeThroughAction";
        Action action = instance.getStrikeThroughAction();
        assertEquals(action.getClass().getName(), className);
    }

    /**
     * Test of getUnderlineAction method, of class EditorActionManager.
     */
    @Test
    public void testGetUnderlineAction() {
        System.out.println("getUnderlineAction");
        
        String className = "com.jmonkey.office.jwp.support.EditorActionManager$UnderlineAction";
        Action action = instance.getUnderlineAction();
        assertEquals(action.getClass().getName(), className);
    }

    /**
     * Test of getUndoAction method, of class EditorActionManager.
     */
    @Test
    public void testGetUndoAction() {
        System.out.println("getUndoAction");
        
        String className = "com.jmonkey.office.jwp.support.EditorActionManager$UndoAction";
        Action action = instance.getUndoAction();
        assertEquals(action.getClass().getName(), className);
    }

    /**
     * Test of isActiveEditor method, of class EditorActionManager.
     */
    @Test
    public void testIsActiveEditor() {
        System.out.println("isActiveEditor");
        
        assert(EditorActionManager.isActiveEditor());
        try {
            editorField.set(instance, null);
        } catch (IllegalArgumentException ex) {
            Logger.getLogger(EditorActionManagerTest.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(EditorActionManagerTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        assert(!EditorActionManager.isActiveEditor());
    }

    /**
     * Test of setCharacterAttributes method, of class EditorActionManager.
     */
    @Test
    public void testSetCharacterAttributes() {
        System.out.println("setCharacterAttributes");
        
        Editor editor = Editor.createEditorForContentType("text/plain", app);
        instance.activate(editor);
        
        // Try setting the cursor to bold.
        MutableAttributeSet attr = new SimpleAttributeSet();
        StyleConstants.setBold(attr, true);
        instance.setCharacterAttributes(editor.getTextComponent(), attr, false);
        
        assert(StyleConstants.isBold(editor.getInputAttributes()));
        
        // Try replacing bold with italic.
        StyleConstants.setBold(attr, false);
        StyleConstants.setItalic(attr, true);
        instance.setCharacterAttributes(editor.getTextComponent(), attr, true);
        assert(!StyleConstants.isBold(editor.getInputAttributes()));
        assert(StyleConstants.isItalic(editor.getInputAttributes()));
        
        // Reset attributes
        StyleConstants.setBold(attr, false);
        StyleConstants.setItalic(attr, false);
        instance.setCharacterAttributes(editor.getTextComponent(), attr, true);
        
        // Try setting selection to bold.
        JEditorPane textComponent = editor.getTextComponent();
        textComponent.setText("This is some text.");
        textComponent.selectAll();
        
        StyleConstants.setBold(attr, true);
        instance.setCharacterAttributes(editor.getTextComponent(), attr, true);
        
        // Verify that the selected text has become bold.
        int p0 = textComponent.getSelectionStart();
        int p1 = textComponent.getSelectionEnd();
        Document doc = textComponent.getDocument();
        while (p0 != p1) {
            Element element = ((StyledDocument) doc).getCharacterElement(p0);
            assert(StyleConstants.isBold(element.getAttributes()));
            p0++;
        }
    }

    /**
     * Test of getCharacterAttributes method, of class EditorActionManager.
     */
    @Test
    public void testGetCharacterAttributes() {
        System.out.println("getCharacterAttributes");
        
        // Get the bold attribute.
        Editor editor = EditorActionManager.getActiveEditor();
        JEditorPane textComponent = editor.getTextComponent();
        MutableAttributeSet attr = new SimpleAttributeSet();
        StyleConstants.setBold(attr, true);
        instance.setCharacterAttributes(textComponent, attr, true);
        assert(StyleConstants.isBold(instance.getCharacterAttributes(textComponent)));
        
        // Get italic selection.
        textComponent.setText("This is some italic text.");
        textComponent.selectAll();
        StyleConstants.setBold(attr, false);
        StyleConstants.setItalic(attr, true);
        instance.setCharacterAttributes(textComponent, attr, true);
        assert(StyleConstants.isItalic(instance.getCharacterAttributes(textComponent)));
    }

    /**
     * Test of setParagraphAttributes method, of class EditorActionManager.
     */
    @Test
    public void testSetParagraphAttributes() {
        System.out.println("setParagraphAttributes");
        
        Editor editor = Editor.createEditorForContentType("text/plain", app);
        instance.activate(editor);
        JEditorPane textComponent = editor.getTextComponent();
        
        String paragraphOne = "This is the first paragraph.";
        textComponent.setText(paragraphOne);
        MutableAttributeSet attr = new SimpleAttributeSet();
        StyleConstants.setAlignment(attr, StyleConstants.ALIGN_CENTER);
        instance.setParagraphAttributes(textComponent, attr, false);
        
        // Test center alignment of one paragraph.
        StyledDocument doc = (StyledDocument) textComponent.getDocument();
        for (int i = 0; i < paragraphOne.length(); i++) {
            Element element = doc.getParagraphElement(i);
            assert(element.getAttributes().containsAttribute(StyleConstants.Alignment, StyleConstants.ALIGN_CENTER));
        }
        
        // Test replace.
        StyleConstants.setAlignment(attr, StyleConstants.ALIGN_LEFT);
        instance.setParagraphAttributes(textComponent, attr, true);
        for (int i = 0; i < paragraphOne.length(); i++) {
            Element element = doc.getParagraphElement(i);
            assert(element.getAttributes().containsAttribute(StyleConstants.Alignment, StyleConstants.ALIGN_LEFT));
        }
        
        // Test right alignment of multiple paragraphs,
        // by using selection.
        String paragraphTwo = "This is the second paragraph.";
        textComponent.setText(paragraphOne + "\n\n" + paragraphTwo);
        textComponent.selectAll();
        
        attr = new SimpleAttributeSet();
        StyleConstants.setAlignment(attr, StyleConstants.ALIGN_RIGHT);
        instance.setParagraphAttributes(textComponent, attr, true);
        
        int textLength = textComponent.getText().length();
        for (int i = 0; i < textLength; i++) {
            Element element = doc.getParagraphElement(i);
            assert(element.getAttributes().containsAttribute(StyleConstants.Alignment, StyleConstants.ALIGN_RIGHT));
        }
    }

    /**
     * Test of threads method, of class EditorActionManager.
     */
    @Test
    public void testThreads() {
        System.out.println("threads");
        Runnable r = new Runnable() {
            @Override
            public void run() {
                System.out.println("Test thread.");
            }
        };
        
        Runnable result = EditorActionManager.threads(r);
        assertNotNull(result);
        assert(((Thread)result).isAlive());
    }
    
    /**
     * Test of FontFamilyAction subclass, of class EditorActionManager.
     */
    @Test
    public void testFontFamilyAction() {
        Editor editor = EditorActionManager.getActiveEditor();
        ActionEvent event = new ActionEvent(editor, 0, "Arial");
        instance.getFontFaceAction("Arial").actionPerformed(event);
        
        AttributeSet attr = instance.getCharacterAttributes(editor.getTextComponent());
        assert(attr.containsAttribute(StyleConstants.Family, "Arial"));
    }
    
    /**
     * Test of FontSizeAction subclass, of class EditorActionManager.
     */
    @Test
    public void testFontSizeAction() {
        Editor editor = EditorActionManager.getActiveEditor();
        ActionEvent event = new ActionEvent(editor, 0, "5");
        instance.getFontSizeAction(5).actionPerformed(event);
        
        AttributeSet attr = instance.getCharacterAttributes(editor.getTextComponent());
        assert(attr.containsAttribute(StyleConstants.FontSize, 5));
    }
    
    /**
     * Test of ForegroundAction subclass, of class EditorActionManager.
     */
    @Test
    public void testForegroundAction() {
        Editor editor = EditorActionManager.getActiveEditor();
        ActionEvent event = new ActionEvent(editor, 0, "0xFF0000");
        instance.getColourAction("Red", Color.red).actionPerformed(event);
        
        AttributeSet attr = instance.getCharacterAttributes(editor.getTextComponent());
        assert(attr.containsAttribute(StyleConstants.Foreground, Color.red));
    }
    
    /**
     * Test of AlignmentAction subclass, of class EditorActionManager.
     */
    @Test
    public void testAlignmentAction() {
        Editor editor = EditorActionManager.getActiveEditor();
        ActionEvent event = new ActionEvent(editor, 0, new Integer(StyleConstants.ALIGN_CENTER).toString());
        instance.getAlignCenterAction().actionPerformed(event);
        
        AttributeSet attr = instance.getCharacterAttributes(editor.getTextComponent());
        assert(attr.containsAttribute(StyleConstants.Alignment, StyleConstants.ALIGN_CENTER));
    }
    
    /**
     * Test of BoldAction subclass, of class EditorActionManager.
     */
    @Test
    public void testBoldAction() {
        Editor editor = EditorActionManager.getActiveEditor();
        ActionEvent event = new ActionEvent(editor, 0, "");
        instance.getBoldAction().actionPerformed(event);
        
        AttributeSet attr = instance.getCharacterAttributes(editor.getTextComponent());
        assert(StyleConstants.isBold(attr));
    }
    
    /**
     * Test of ItalicAction subclass, of class EditorActionManager.
     */
    @Test
    public void testItalicAction() {
        Editor editor = EditorActionManager.getActiveEditor();
        ActionEvent event = new ActionEvent(editor, 0, "");
        instance.getItalicAction().actionPerformed(event);
        
        AttributeSet attr = instance.getCharacterAttributes(editor.getTextComponent());
        assert(StyleConstants.isItalic(attr));
    }
    
    /**
     * Test of UnderlineAction subclass, of class EditorActionManager.
     */
    @Test
    public void testUnderlineAction() {
        Editor editor = EditorActionManager.getActiveEditor();
        ActionEvent event = new ActionEvent(editor, 0, "");
        instance.getUnderlineAction().actionPerformed(event);
        
        AttributeSet attr = instance.getCharacterAttributes(editor.getTextComponent());
        assert(StyleConstants.isUnderline(attr));
    }
    
    /**
     * Test of StrikeThroughAction subclass, of class EditorActionManager.
     */
    @Test
    public void testStrikeThroughAction() {
        Editor editor = EditorActionManager.getActiveEditor();
        ActionEvent event = new ActionEvent(editor, 0, "");
        instance.getStrikeThroughAction().actionPerformed(event);
        
        AttributeSet attr = instance.getCharacterAttributes(editor.getTextComponent());
        assert(StyleConstants.isStrikeThrough(attr));
    }
    
    /**
     * Test of SelectAllAction subclass, of class EditorActionManager.
     */
    @Test
    public void testSelectAllAction() {
        Editor editor = Editor.createEditorForContentType("text/plain", app);
        instance.activate(editor);
        JEditorPane textComponent = editor.getTextComponent();
        textComponent.setText("Test");
        
        ActionEvent event = new ActionEvent(editor, 0, "");
        instance.getSelectAllAction().actionPerformed(event);
        
        assertEquals(textComponent.getSelectedText(), "Test");
    }
    
    /**
     * Test of SelectNoneAction subclass, of class EditorActionManager.
     */
    @Test
    public void testSelectNoneAction() {
        Editor editor = Editor.createEditorForContentType("text/plain", app);
        instance.activate(editor);
        
        JEditorPane textComponent = editor.getTextComponent();
        textComponent.setText("Test");
        textComponent.selectAll();
        
        ActionEvent event = new ActionEvent(editor, 0, "");
        instance.getSelectNoneAction().actionPerformed(event);
        
        assertNull(textComponent.getSelectedText());
    }
}
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jmonkey.office.jwp.support.editors;

// Additional package for testing
import com.jmonkey.export.RegistryFormatException;
import com.jmonkey.office.jwp.DocumentManager;
import com.jmonkey.office.jwp.support.EditorActionManager;
import com.jmonkey.office.jwp.JWP;
import java.io.IOException;
import java.io.FileNotFoundException;

import com.jmonkey.office.jwp.support.Editor;
import java.awt.Button;
import java.awt.Color;
import java.awt.Component;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JEditorPane;
import javax.swing.text.Element;
import javax.swing.text.MutableAttributeSet;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author tri
 */
public class TEXTEditorTest {

    private JWP app;
    private DocumentManager dm;
    private EditorActionManager eam;
    
    public TEXTEditorTest() throws RegistryFormatException {
        app = new JWP(null);
        dm = new DocumentManager(app);
        eam = new EditorActionManager(app, dm);        
    }


    /**
     * Test of append method, of class TEXTEditor.
     */
    @Test
    public void testAppend() throws Exception {
        System.out.println("append");
        File file = null;
        TEXTEditor instance = new TEXTEditor(eam);
        try{
            instance.append(file);
        } catch(NullPointerException e){
            System.out.println("Caught right exception");
        } catch(Exception e){
            fail("Caught incorrect exception");
        }
        
        file = new File("test.txt");
        try {
            file.createNewFile();
        } catch (IOException ex) {
            Logger.getLogger(StyledEditorTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        try{
            instance.append(file);
        } catch(IOException e){
            System.out.println("Approriate exception caught");
        } catch(Exception e){
            fail("Caught incorrect exception");
        }
    }

    /**
     * Test of documentSetSelection method, of class TEXTEditor.
     */
    @Test
    public void testDocumentSetSelection() {
        System.out.println("documentSetSelection");
        int start = 0;
        int length = 0;
        boolean wordsOnly = false;
        TEXTEditor instance = new TEXTEditor(eam);

        try{
            instance.documentSetSelection(start, length, wordsOnly);
        } catch(Exception e){
            fail("Caught incorrect exception");
        }        
    }

    /**
     * Test of getContentType method, of class TEXTEditor.
     */
    @Test
    public void testGetContentType() {
        System.out.println("getContentType");
        TEXTEditor instance = new TEXTEditor(eam);
        String result = instance.getContentType();
        assertNotNull(result);
    }

    /**
     * Test of getCurrentParagraph method, of class TEXTEditor.
     */
    @Test
    public void testGetCurrentParagraph() {
        System.out.println("getCurrentParagraph");
        TEXTEditor instance = new TEXTEditor(eam);
        Element expResult = null;
        Element result = instance.getCurrentParagraph();
        assertEquals(expResult, result);
    }

    /**
     * Test of getCurrentRun method, of class TEXTEditor.
     */
    @Test
    public void testGetCurrentRun() {
        System.out.println("getCurrentRun");
        TEXTEditor instance = new TEXTEditor(eam);
        Element result = instance.getCurrentRun();
        assertNull(result);
    }

    /**
     * Test of getFileExtensions method, of class TEXTEditor.
     */
    @Test
    public void testGetFileExtensions() {
        System.out.println("getFileExtensions");
        TEXTEditor instance = new TEXTEditor(eam);
        String[] result = instance.getFileExtensions();
        assertNotNull(result);
    }

    /**
     * Test of getInputAttributes method, of class TEXTEditor.
     */
    @Test
    public void testGetInputAttributes() {
        System.out.println("getInputAttributes");
        TEXTEditor instance = new TEXTEditor(eam);
        MutableAttributeSet result = instance.getInputAttributes();
        assertNotNull(result);
    }

    /**
     * Test of getTextComponent method, of class TEXTEditor.
     */
    @Test
    public void testGetTextComponent() {
        System.out.println("getTextComponent");
        TEXTEditor instance = new TEXTEditor(eam);
        JEditorPane result = instance.getTextComponent();
        assertNotNull(result);
    }

    /**
     * Test of hasBeenActivated method, of class TEXTEditor.
     */
    @Test
    public void testHasBeenActivated() {
        System.out.println("hasBeenActivated");
        Editor editor = null;
        TEXTEditor instance = new TEXTEditor(eam);
        try{
            instance.hasBeenActivated(editor);
        } catch(Exception e){
            fail("Exception thrown");
        }
        File file = new File("test.txt");
        instance.setFile(file);
        editor = instance;
        try{
            instance.hasBeenActivated(editor);
        } catch(Exception e){
            fail("Exception thrown");
        }
        
        try {
            boolean f1 = file.createNewFile();
        } catch (IOException ex) {
            Logger.getLogger(StyledEditorTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        instance.setFile(file);
        editor = instance;
        try{
            instance.hasBeenActivated(editor);
        } catch(Exception e){
            fail("Exception thrown");
        }
        
        instance.setChanged(true);
        editor = instance;
        try{
            instance.hasBeenActivated(editor);
        } catch(Exception e){
            fail("Exception thrown");
        }
        file.delete();
    }

    /**
     * Test of hasBeenDeactivated method, of class TEXTEditor.
     */
    @Test
    public void testHasBeenDeactivated() {
        System.out.println("hasBeenDeactivated");
        TEXTEditor instance = new TEXTEditor(eam);

        // Test if it can handle null editor input
        Editor editor = null;
        try{
            instance.hasBeenDeactivated(editor);
        } catch(Exception e){
            fail("Exception thrown");
        }
        
        // Test if it can run without exception
        editor = instance;
        File file = new File("test.txt");
        instance.setFile(file);
        try{
            instance.hasBeenDeactivated(editor);
        } catch(Exception e){
            fail("Exception thrown");
        }
    }

    /**
     * Test of init method, of class TEXTEditor.
     */
    @Test
    public void testInit() {
        System.out.println("init");
        TEXTEditor instance = new TEXTEditor(eam);
        JEditorPane result = instance.getTextComponent();
        instance.init();
        assertNotNull(result);
    }

    /**
     * Test of insert method, of class TEXTEditor.
     */
    @Test
    public void testInsert() throws Exception {
        System.out.println("insert");
        TEXTEditor instance = new TEXTEditor(eam);

        // Test with null file
        File file = null;
        int position = 0;
        try{
            instance.insert(file, position);
        } catch(Exception e){
            fail("Exception thrown");
        }

        file = new File("test.txt");
        try {
            file.createNewFile();
        } catch (IOException ex) {
            Logger.getLogger(StyledEditorTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        try{
            instance.insert(file, position);
        } catch(IOException e){
            System.out.println("Approriate exception caught");
        } catch(Exception e){
            fail("Caught incorrect exception");
        }
        //file.delete();
    }

    /**
     * Test of isChanged method, of class TEXTEditor.
     */
    @Test
    public void testIsChanged() {
        System.out.println("isChanged");
        // New instance, isChange should be false
        TEXTEditor instance = new TEXTEditor(eam);
        boolean expResult = false;
        boolean result = instance.isChanged();
        assertEquals(expResult, result);
        
        // Change something, isChasnge should be true
        expResult = true;
        instance.setChanged(expResult);
        result = instance.isChanged();
        assertEquals(expResult, result);
    }

    /**
     * Test of isEmpty method, of class TEXTEditor.
     */
    @Test
    public void testIsEmpty() {
        System.out.println("isEmpty");
        TEXTEditor instance = new TEXTEditor(eam);
        boolean expResult = true;
        boolean result = instance.isEmpty();
        assertEquals(expResult, result);
    }

    /**
     * Test of isFormatted method, of class TEXTEditor.
     */
    @Test
    public void testIsFormatted() {
        System.out.println("isFormatted");
        TEXTEditor instance = new TEXTEditor(eam);
        boolean expResult = false;
        boolean result = instance.isFormatted();
        assertEquals(expResult, result);
    }

    /**
     * Test of isNew method, of class TEXTEditor.
     */
    @Test
    public void testIsNew() {
        System.out.println("isNew");
        TEXTEditor instance = new TEXTEditor(eam);
        boolean expResult = true;
        boolean result = instance.isNew();
        assertEquals(expResult, result);
    }

    /**
     * Test of keyPressed method, of class TEXTEditor.
     */
    @Test
    public void testKeyPressed() {
        System.out.println("keyPressed");
        TEXTEditor instance = new TEXTEditor(eam);

        // Try passing null keyevent
        KeyEvent kp = null;
        try{
            instance.keyPressed(kp);
        } catch(Exception e){
            System.out.println("Good exception thrown");
        }
        
        Component p = new Button();
        kp = new KeyEvent(p, 2, 22, 10, KeyEvent.VK_T);
        instance.keyPressed(kp);
        //kp = new KeyEvent(p, 2, 22, 10, KeyEvent.VK_TAB);
        instance.keyPressed(kp);  
    }

    /**
     * Test of keyReleased method, of class TEXTEditor.
     */
    @Test
    public void testKeyReleased() {
        System.out.println("keyReleased");
        KeyEvent kr = null;
        TEXTEditor instance = new TEXTEditor(eam);

        try{
            instance.keyReleased(kr);
        } catch(Exception e){
            System.out.println("Good exception thrown");
        }
    }

    /**
     * Test of keyTyped method, of class TEXTEditor.
     */
    @Test
    public void testKeyTyped() {
        System.out.println("keyTyped");
        KeyEvent kt = null;
        TEXTEditor instance = new TEXTEditor(eam);

        try{
            instance.keyTyped(kt);
        } catch(Exception e){
            System.out.println("Good exception thrown");
        }
    }

    /**
     * Test of mouseClicked method, of class TEXTEditor.
     */
    @Test
    public void testMouseClicked() {
        System.out.println("mouseClicked");
        MouseEvent e = null;
        TEXTEditor instance = new TEXTEditor(eam);

        try{
            instance.mouseClicked(e);
        } catch(Exception ex){
            System.out.println("Good exception thrown");
        }
        Component p = new Button();
        e = new MouseEvent(p, 2, 22, 10, 10, 10, 1, false, MouseEvent.BUTTON1);
        instance.mouseClicked(e);
        e = new MouseEvent(p, 2, 22, 10, 10, 10, 1, false, MouseEvent.BUTTON2);
        instance.mouseClicked(e);
        e = new MouseEvent(p, 2, 22, 10, 10, 10, 1, false, MouseEvent.BUTTON3);
        instance.mouseClicked(e);
    }

    /**
     * Test of mouseEntered method, of class TEXTEditor.
     */
    @Test
    public void testMouseEntered() {
        System.out.println("mouseEntered");
        MouseEvent e = null;
        TEXTEditor instance = new TEXTEditor(eam);

        try{
            instance.mouseEntered(e);
        } catch(Exception ex){
            System.out.println("Good exception thrown");
        }
    }

    /**
     * Test of mouseExited method, of class TEXTEditor.
     */
    @Test
    public void testMouseExited() {
        System.out.println("mouseExited");
        MouseEvent e = null;
        TEXTEditor instance = new TEXTEditor(eam);
        try{
            instance.mouseExited(e);
        } catch(Exception ex){
            System.out.println("Good exception thrown");
        }
    }

    /**
     * Test of mousePressed method, of class TEXTEditor.
     */
    @Test
    public void testMousePressed() {
        System.out.println("mousePressed");
        MouseEvent e = null;
        TEXTEditor instance = new TEXTEditor(eam);

        try{
            instance.mousePressed(e);
        } catch(Exception ex){
            System.out.println("Good exception thrown");
        }
    }

    /**
     * Test of mouseReleased method, of class TEXTEditor.
     */
    @Test
    public void testMouseReleased() {
        System.out.println("mouseReleased");
        MouseEvent e = null;
        TEXTEditor instance = new TEXTEditor(eam);

        try{
            instance.mouseReleased(e);
        } catch(Exception ex){
            System.out.println("Good exception thrown");
        }
    }

    /**
     * Test of read method, of class TEXTEditor.
     */
    @Test
    public void testRead() throws Exception {
        System.out.println("read");
        TEXTEditor instance = new TEXTEditor(eam);
        
        // Test with null file
        File file = null;
        try{
            instance.read(file);
        } catch(Exception ex){
            System.out.println("Good exception thrown");
        }
        
        file = new File("test.txt");
        try {
            file.createNewFile();
        } catch (IOException ex) {
            Logger.getLogger(StyledEditorTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        try{
            instance.read(file);
        } catch(IOException e){
            System.out.println("Approriate exception caught");
        } catch(Exception e){
            fail("Caught incorrect exception");
        }   
        //file.delete();
    }

    /**
     * Test of requestFocus method, of class TEXTEditor.
     */
    @Test
    public void testRequestFocus() {
        System.out.println("requestFocus");
        TEXTEditor instance = new TEXTEditor(eam);
        try{
            instance.requestFocus();
        } catch(Exception ex){
            fail("Bad exception");
        }
    }

    /**
     * Test of setCaretBlinkRate method, of class TEXTEditor.
     */
    @Test
    public void testSetCaretBlinkRate() {
        System.out.println("setCaretBlinkRate");
        int rate = 0;
        TEXTEditor instance = new TEXTEditor(eam);
        try{
            instance.setCaretBlinkRate(rate);
        } catch(Exception ex){
            fail("Bad exception");
        }
    }

    /**
     * Test of setCaretColor method, of class TEXTEditor.
     */
    @Test
    public void testSetCaretColor() {
        System.out.println("setCaretColor");
        Color colour = null;
        TEXTEditor instance = new TEXTEditor(eam);
        
        try{
            instance.setCaretColor(colour);
        } catch(Exception ex){
            System.out.println("Approriate exception caught");
        }
    }

    /**
     * Test of setChanged method, of class TEXTEditor.
     */
    @Test
    public void testSetChanged() {
        System.out.println("setChanged");
        boolean changed = false;
        TEXTEditor instance = new TEXTEditor(eam);
        try{
            instance.setChanged(changed);
        } catch(Exception ex){
            fail("Bad exception");
        }
        boolean result = instance.isChanged();
        assertEquals(result, changed);
        changed = true;
        try{
            instance.setChanged(changed);
        } catch(Exception ex){
            fail("Bad exception");
        }
        result = instance.isChanged();
        assertEquals(result, changed);
    }

    /**
     * Test of setCurrentParagraph method, of class TEXTEditor.
     */
    @Test
    public void testSetCurrentParagraph() {
        System.out.println("setCurrentParagraph");
        Element paragraph = null;
        TEXTEditor instance = new TEXTEditor(eam);
        try{
            instance.setCurrentParagraph(paragraph);
        } catch(Exception ex){
            System.out.println("Approriate exception caught");
        }
    }

    /**
     * Test of setCurrentRun method, of class TEXTEditor.
     */
    @Test
    public void testSetCurrentRun() {
        System.out.println("setCurrentRun");
        Element run = null;
        TEXTEditor instance = new TEXTEditor(eam);
        
        try{
            instance.setCurrentRun(run);
        } catch(Exception ex){
            System.out.println("Approriate exception caught");
        }
    }

    /**
     * Test of setSelectionColor method, of class TEXTEditor.
     */
    @Test
    public void testSetSelectionColor() {
        System.out.println("setSelectionColor");
        Color colour = null;
        TEXTEditor instance = new TEXTEditor(eam);
        
        try{
            instance.setSelectionColor(colour);
        } catch(Exception ex){
            System.out.println("Approriate exception caught");
        }
    }

    /**
     * Test of write method, of class TEXTEditor.
     */
    @Test
    public void testWrite() throws Exception {
        System.out.println("write");
        File file = null;
        TEXTEditor instance = new TEXTEditor(eam);

        try{
            instance.write(file);
        } catch(Exception ex){
            System.out.println("Good exception thrown");
        }
        
        file = new File("test.txt");
        try {
            file.createNewFile();
        } catch (IOException ex) {
            Logger.getLogger(StyledEditorTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        try{
            instance.write(file);
        } catch(IOException e){
            System.out.println("Approriate exception caught");
        } catch(Exception e){
            fail("Caught incorrect exception");
        }
        //file.delete();
    }
}

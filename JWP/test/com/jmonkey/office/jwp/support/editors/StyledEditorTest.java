/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jmonkey.office.jwp.support.editors;

import com.jmonkey.office.jwp.DocumentManager;
import com.jmonkey.office.jwp.support.Editor;
import com.jmonkey.office.jwp.support.EditorActionManager;
import com.jmonkey.office.jwp.JWP;
import java.awt.Button;
import java.awt.Color;
import java.awt.Component;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.io.File;
import javax.swing.JEditorPane;
import javax.swing.text.Element;
import javax.swing.text.MutableAttributeSet;
import org.junit.Test;
import static org.junit.Assert.*;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.text.SimpleAttributeSet;

/**
 *
 * @author mackenziemarshall
 */
public class StyledEditorTest {
    private static final String IO_TEST_FILE = "test\\com\\jmonkey\\office\\jwp\\support\\editors\\ioTestFile.txt";
    
    private JWP app;
    private DocumentManager dm;
    private EditorActionManager eam;
    
    public StyledEditorTest() throws Exception {
        app = new JWP(null);
        dm = new DocumentManager(app);
        eam = new EditorActionManager(app, dm);
    }

    /**
     * Test of append method, of class StyledEditor.
     */
    @Test
    public void testAppend() throws Exception {
        System.out.println("append");
        
        // Test with null file.
        HTMLEditor instance = new HTMLEditor(eam);
        try{
            instance.append(null);
        } catch(NullPointerException e){
            System.out.println("Caught right exception");
        } catch(Exception e){
            fail("Caught incorrect exception");
        }
        
        File file = new File(IO_TEST_FILE);
        try{
            instance.append(file);
        } catch(NullPointerException e){
            System.out.println("Caught right exception");
        } catch(Exception e){
            fail("Caught incorrect exception");
        }
        
        //file = new File("bob.txt");
//        try{
//            instance.append(file);
//        } catch(NullPointerException e){
//            System.out.println("Caught right exception");
//        } catch(Exception e){
//            fail("Caught incorrect exception");
//        }
    }

    /**
     * Test of hasBeenActivated method, of class StyledEditor.
     */
    @Test
    public void testHasBeenActivated() {
        System.out.println("hasBeenActivated");
        Editor editor = null;
        HTMLEditor instance = new HTMLEditor(eam);
        try{
            instance.hasBeenActivated(editor);
        } catch(Exception e){
            fail("Exception thrown");
        }
        File file = new File("test.txt");
        instance.setFile(file);
        editor = instance;
        try{
            instance.hasBeenActivated(editor);
        } catch(Exception e){
            fail("Exception thrown");
        }
        
        try {
            boolean f1 = file.createNewFile();
        } catch (IOException ex) {
            Logger.getLogger(StyledEditorTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        instance.setFile(file);
        editor = instance;
        try{
            instance.hasBeenActivated(editor);
        } catch(Exception e){
            fail("Exception thrown");
        }
        
        instance.setChanged(true);
        editor = instance;
        try{
            instance.hasBeenActivated(editor);
        } catch(Exception e){
            fail("Exception thrown");
        }
        file.delete();
    }

    /**
     * Test of insert method, of class StyledEditor.
     */
    @Test
    public void testInsert() throws Exception {
        System.out.println("insert");
        
        // Invalid file.
        int position = 0;
        HTMLEditor instance = new HTMLEditor(eam);
        try{
            instance.insert(null, position);
        } catch(IOException e){
            System.out.println("Approriate exception caught");
        } catch(Exception e){
            fail("Caught incorrect exception");
        }
        
        File file = new File(IO_TEST_FILE);
        position = -1;
        
        // Invalid file position.
        try{
            instance.insert(file, position);
        } catch(IOException e){
            System.out.println("Approriate exception caught");
        } catch(Exception e){
            fail("Caught incorrect exception");
        }
        
        // Valid file and position.
        position = 0;
        try{
            instance.insert(file, position);
        } catch(IOException e){
            System.out.println("Approriate exception caught");
        } catch(Exception e){
            fail("Caught incorrect exception");
        }
    }

    /**
     * Test of read method, of class StyledEditor.
     */
    @Test
    public void testRead() throws Exception {
        System.out.println("read");
        
        // Test invalid file.
        HTMLEditor instance = new HTMLEditor(eam);
        try{
            instance.read(null);
        } catch(IOException e){
            System.out.println("Approriate exception caught");
        } catch(Exception e){
            fail("Caught incorrect exception");
        }
        
        File file = new File(IO_TEST_FILE);
        try{
            instance.read(file);
        } catch(IOException e){
            System.out.println("Approriate exception caught");
        } catch(Exception e){
            fail("Caught incorrect exception");
        }
    }

    /**
     * Test of write method, of class StyledEditor.
     */
    @Test
    public void testWrite() throws Exception {
        System.out.println("write");
        
        // Test invalid file.
        HTMLEditor instance = new HTMLEditor(eam);
        try{
            instance.write(null);
        } catch(IOException e){
            System.out.println("Approriate exception caught");
        } catch(Exception e){
            fail("Caught incorrect exception");
        }
        
        File file = new File(IO_TEST_FILE);
        try{
            instance.write(file);
        } catch(IOException e){
            System.out.println("Approriate exception caught");
        } catch(Exception e){
            fail("Caught incorrect exception");
        }
    }

    /**
     * Test of documentSetSelection method, of class StyledEditor.
     */
    @Test
    public void testDocumentSetSelection() {
        System.out.println("documentSetSelection");
        int start = -1;
        int length = 0;
        boolean wordsOnly = false;
        HTMLEditor instance = new HTMLEditor(eam);
        instance.documentSetSelection(start, length, wordsOnly);
        start = 0;
        length = -1;
        instance.documentSetSelection(start, length, wordsOnly);
        length = 0;
        instance.documentSetSelection(start, length, wordsOnly);
    }

    /**
     * Test of getContentType method, of class StyledEditor.
     */
    @Test
    public void testGetContentType() {
        System.out.println("getContentType");
        HTMLEditor instance = new HTMLEditor(eam);
        String expResult = "text/html";
        String result = instance.getContentType();
        assertEquals(expResult, result); 
    }

    /**
     * Test of getCurrentParagraph method, of class StyledEditor.
     */
    @Test
    public void testGetCurrentParagraph() {
        System.out.println("getCurrentParagraph");
        HTMLEditor instance = new HTMLEditor(eam);
        Element expResult = null;
        Element result = instance.getCurrentParagraph();
        assertEquals(expResult, result);
    }

    /**
     * Test of getCurrentRun method, of class StyledEditor.
     */
    @Test
    public void testGetCurrentRun() {
        System.out.println("getCurrentRun");
        HTMLEditor instance = new HTMLEditor(eam);
        Element expResult = null;
        Element result = instance.getCurrentRun();
        assertEquals(expResult, result);
    }

    /**
     * Test of getInputAttributes method, of class StyledEditor.
     */
    @Test
    public void testGetInputAttributes() {
        System.out.println("getInputAttributes");
        HTMLEditor instance = new HTMLEditor(eam);
        MutableAttributeSet result = instance.getInputAttributes();
        assertNotNull(result);
    }

    /**
     * Test of getTextComponent method, of class StyledEditor.
     */
    @Test
    public void testGetTextComponent() {
        System.out.println("getTextComponent");
        HTMLEditor instance = new HTMLEditor(eam);
        JEditorPane result = instance.getTextComponent();
        assertNotNull(result);
    }

    /**
     * Test of hasBeenDeactivated method, of class StyledEditor.
     */
    @Test
    public void testHasBeenDeactivated() {
        System.out.println("hasBeenDeactivated");
        Editor editor = null;
        HTMLEditor instance = new HTMLEditor(eam);
        instance.hasBeenDeactivated(editor);
        editor = instance;
        instance.hasBeenDeactivated(editor);
    }

    /**
     * Test of init method, of class StyledEditor.
     */
    @Test
    public void testInit() {
        System.out.println("init");
        HTMLEditor instance = new HTMLEditor(eam);
        JEditorPane result;
        instance.init();
        result = instance.getTextComponent();
        assertNotNull(result);
    }

    /**
     * Test of isChanged method, of class StyledEditor.
     */
    @Test
    public void testIsChanged() {
        System.out.println("isChanged");
        HTMLEditor instance = new HTMLEditor(eam);
        boolean expResult = false;
        boolean result = instance.isChanged();
        assertEquals(expResult, result);
    }

    /**
     * Test of isEmpty method, of class StyledEditor.
     */
    @Test
    public void testIsEmpty() {
        System.out.println("isEmpty");
        HTMLEditor instance = new HTMLEditor(eam);
        boolean expResult = false;
        boolean result = instance.isEmpty();
        assertEquals(expResult, result);
    }

    /**
     * Test of isFormatted method, of class StyledEditor.
     */
    @Test
    public void testIsFormatted() {
        System.out.println("isFormatted");
        HTMLEditor instance = new HTMLEditor(eam);
        boolean expResult = false;
        boolean result = instance.isFormatted();
        assertEquals(expResult, result);
    }

    /**
     * Test of isNew method, of class StyledEditor.
     */
    @Test
    public void testIsNew() {
        System.out.println("isNew");
        HTMLEditor instance = new HTMLEditor(eam);
        boolean expResult = true;
        boolean result = instance.isNew();
        assertEquals(expResult, result);
    }

    /**
     * Test of keyPressed method, of class StyledEditor.
     */
    @Test
    public void testKeyPressed() {
        System.out.println("keyPressed");
        Component p = new Button();
        KeyEvent kp = new KeyEvent(p, 2, 22, 10, KeyEvent.VK_T);
        HTMLEditor instance = new HTMLEditor(eam);
        instance.keyPressed(kp);
        kp = new KeyEvent(p, 2, 22, 10, KeyEvent.VK_TAB);
        instance.keyPressed(kp);
    }

    /**
     * Test of keyReleased method, of class StyledEditor.
     */
    @Test
    public void testKeyReleased() {
        System.out.println("keyReleased");
        Component p = new Button();
        KeyEvent kr = new KeyEvent(p, 2, 22, 10, KeyEvent.VK_A);
        HTMLEditor instance = new HTMLEditor(eam);
        instance.keyReleased(kr);
        kr = null;
        instance.keyReleased(kr);
    }

    /**
     * Test of keyTyped method, of class StyledEditor.
     */
    @Test
    public void testKeyTyped() {
        System.out.println("keyTyped");
        Component p = new Button();
        KeyEvent kt = new KeyEvent(p, 2, 22, 10, KeyEvent.VK_A);
        HTMLEditor instance = new HTMLEditor(eam);
        instance.keyTyped(kt);
        kt = null;
        instance.keyTyped(kt);
    }

    /**
     * Test of mouseClicked method, of class StyledEditor.
     */
    @Test
    public void testMouseClicked() {
        System.out.println("mouseClicked");
        Component p = new Button();
        MouseEvent e = new MouseEvent(p, 2, 22, 10, 10, 10, 1, false, MouseEvent.BUTTON1);
        HTMLEditor instance = new HTMLEditor(eam);
        instance.mouseClicked(e);
        e = new MouseEvent(p, 2, 22, 10, 10, 10, 1, false, MouseEvent.BUTTON2);
        instance.mouseClicked(e);
        e = new MouseEvent(p, 2, 22, 10, 10, 10, 1, false, MouseEvent.BUTTON3);
        instance.mouseClicked(e);
    }

    /**
     * Test of mouseEntered method, of class StyledEditor.
     */
    @Test
    public void testMouseEntered() {
        System.out.println("mouseEntered");
        Component p = new Button();
        MouseEvent e = new MouseEvent(p, 2, 22, 10, 10, 10, 1, false, MouseEvent.BUTTON1);
        HTMLEditor instance = new HTMLEditor(eam);
        instance.mouseEntered(e);
        e = null;
        instance.mouseEntered(e);
    }

    /**
     * Test of mouseExited method, of class StyledEditor.
     */
    @Test
    public void testMouseExited() {
        System.out.println("mouseExited");
        Component p = new Button();
        MouseEvent e = new MouseEvent(p, 2, 22, 10, 10, 10, 1, false, MouseEvent.BUTTON1);
        HTMLEditor instance = new HTMLEditor(eam);
        instance.mouseExited(e);
        e = null;
        instance.mouseExited(e);
    }

    /**
     * Test of mousePressed method, of class StyledEditor.
     */
    @Test
    public void testMousePressed() {
        System.out.println("mousePressed");
        Component p = new Button();
        MouseEvent e = new MouseEvent(p, 2, 22, 10, 10, 10, 1, false, MouseEvent.BUTTON1);
        HTMLEditor instance = new HTMLEditor(eam);
        instance.mousePressed(e);
        e = null;
        instance.mousePressed(e);
    }

    /**
     * Test of mouseReleased method, of class StyledEditor.
     */
    @Test
    public void testMouseReleased() {
        System.out.println("mouseReleased");
        Component p = new Button();
        MouseEvent e = new MouseEvent(p, 2, 22, 10, 10, 10, 1, false, MouseEvent.BUTTON1);
        HTMLEditor instance = new HTMLEditor(eam);
        instance.mouseReleased(e);
        e = null;
        instance.mouseReleased(e);
    }

    /**
     * Test of requestFocus method, of class StyledEditor.
     */
    @Test
    public void testRequestFocus() {
        System.out.println("requestFocus");
        HTMLEditor instance = new HTMLEditor(eam);
        instance.requestFocus();
    }

    /**
     * Test of setCaretBlinkRate method, of class StyledEditor.
     */
    @Test
    public void testSetCaretBlinkRate() {
        System.out.println("setCaretBlinkRate");
        int rate = 10;
        HTMLEditor instance = new HTMLEditor(eam);
        instance.setCaretBlinkRate(rate);
        assertEquals(10,instance.getTextComponent().getCaret().getBlinkRate());
    }

    /**
     * Test of setCaretColor method, of class StyledEditor.
     */
    @Test
    public void testSetCaretColor() {
        System.out.println("setCaretColor");
        Color colour = Color.BLUE;
        HTMLEditor instance = new HTMLEditor(eam);
        instance.setCaretColor(colour);
        assertEquals(Color.BLUE, instance.getTextComponent().getCaretColor());
    }

    /**
     * Test of setChanged method, of class StyledEditor.
     */
    @Test
    public void testSetChanged() {
        System.out.println("setChanged");
        boolean changed = false;
        HTMLEditor instance = new HTMLEditor(eam);
        instance.setChanged(changed);
        assertEquals(false, instance.isChanged());
    }

    /**
     * Test of setCurrentParagraph method, of class StyledEditor.
     */
    @Test
    public void testSetCurrentParagraph() {
        System.out.println("setCurrentParagraph");
        Element paragraph = null;
        HTMLEditor instance = new HTMLEditor(eam);
        instance.setCurrentParagraph(paragraph);
        assertNull(instance.getCurrentParagraph());
    }

    /**
     * Test of setCurrentRun method, of class StyledEditor.
     */
    @Test
    public void testSetCurrentRun() {
        System.out.println("setCurrentRun");
        Element run = null;
        HTMLEditor instance = new HTMLEditor(eam);
        instance.setCurrentRun(run);
        assertNull(instance.getCurrentRun());
    }

    /**
     * Test of setSelectionColor method, of class StyledEditor.
     */
    @Test
    public void testSetSelectionColor() {
        System.out.println("setSelectionColor");
        Color colour = Color.BLUE;
        HTMLEditor instance = new HTMLEditor(eam);
        instance.setSelectionColor(colour);
        assertEquals(Color.BLUE, instance.getTextComponent().getSelectionColor());
    }
}
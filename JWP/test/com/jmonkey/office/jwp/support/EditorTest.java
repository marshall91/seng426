/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jmonkey.office.jwp.support;

import com.jmonkey.export.Registry;
import com.jmonkey.office.jwp.DocumentManager;
import com.jmonkey.office.jwp.JWP;
import com.jmonkey.office.jwp.support.editors.HTMLEditor;
import com.jmonkey.office.jwp.support.editors.RTFEditor;
import com.jmonkey.office.jwp.support.editors.TEXTEditor;
import java.io.File;
import java.io.IOException;
import javax.swing.JEditorPane;
import javax.swing.JPopupMenu;
import javax.swing.text.Element;
import javax.swing.text.MutableAttributeSet;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.undo.UndoManager;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author mackenziemarshall
 */
public class EditorTest {
    private JWP app;
    private DocumentManager dm;
    private EditorActionManager eam;
    
    public EditorTest() throws Exception{
        app = new JWP(null);
        dm = new DocumentManager(app);
        eam = new EditorActionManager(app, dm);
    }

    /**
     * Test of activate method, of class Editor.
     */
    @Test
    public void testActivate() {
        System.out.println("activate");
        Editor instance = new TEXTEditor(eam);
        instance.activate();
    }

    /**
     * Test of append method, of class Editor.
     */
    @Test
    public void testAppend() throws Exception {
        System.out.println("append");
        File file = null;
        Editor instance = new TEXTEditor(eam);
        instance.append(file);
        
    }

    /**
     * Test of deactivate method, of class Editor.
     */
    @Test
    public void testDeactivate() {
        System.out.println("deactivate");
        Editor instance = new TEXTEditor(eam);
        instance.deactivate();
    }

    /**
     * Test of documentSetSelection method, of class Editor.
     */
    @Test
    public void testDocumentSetSelection() {
        System.out.println("documentSetSelection");
        int start = 0;
        int length = 0;
        boolean wordsOnly = false;
        Editor instance = new TEXTEditor(eam);
        instance.documentSetSelection(start, length, wordsOnly);
        start=-1;
        instance.documentSetSelection(start, length, wordsOnly);
        start=0;
        length=-1;
        instance.documentSetSelection(start, length, wordsOnly);
        length=0;
        wordsOnly = true;
        instance.documentSetSelection(start, length, wordsOnly);
    }

    /**
     * Test of getContentType method, of class Editor.
     */
    @Test
    public void testGetContentType() {
        System.out.println("getContentType");
        Editor instance = new TEXTEditor(eam);
        String expResult = "text/plain";
        String result = instance.getContentType();
        assertEquals(expResult, result);
    }

    /**
     * Test of getCurrentParagraph method, of class Editor.
     */
    @Test
    public void testGetCurrentParagraph() {
        System.out.println("getCurrentParagraph");
        Editor instance = new TEXTEditor(eam);
        Element expResult = null;
        Element result = instance.getCurrentParagraph();
        assertEquals(expResult, result);
    }

    /**
     * Test of getEditorActionManager method, of class Editor.
     */
    @Test
    public void testGetEditorActionManager() {
        System.out.println("getEditorActionManager");
        Editor instance = new TEXTEditor(eam);
        EditorActionManager expResult = eam;
        EditorActionManager result = instance.getEditorActionManager();
        assertEquals(expResult, result);
    }

    /**
     * Test of getCurrentRun method, of class Editor.
     */
    @Test
    public void testGetCurrentRun() {
        System.out.println("getCurrentRun");
        Editor instance = new TEXTEditor(eam);
        Element expResult = null;
        Element result = instance.getCurrentRun();
        assertEquals(expResult, result);
    }

    /**
     * Test of createEditorForContentType method, of class Editor.
     */
    @Test
    public void testCreateEditorForContentType() {
        System.out.println("createEditorForContentType");
        String contentType = "";
        String expResult = "text/plain";
        Editor result = Editor.createEditorForContentType(contentType, app);
        assertEquals(expResult, result.getContentType());
        contentType = "text/plain";
        result = Editor.createEditorForContentType(contentType, app);
        expResult = "text/plain";
        assertEquals(expResult, result.getContentType());
        contentType = "text/html";
        result = Editor.createEditorForContentType(contentType, app);
        expResult = "text/html";
        assertEquals(expResult, result.getContentType());
        contentType = "text/rtf";
        result = Editor.createEditorForContentType(contentType, app);
        expResult = "text/rtf";
        assertEquals(expResult, result.getContentType());
    }

    /**
     * Test of createEditorForExtension method, of class Editor.
     */
    @Test
    public void testCreateEditorForExtension() {
        System.out.println("createEditorForExtension");
        
        String extension = "";
        String expResult = "text/plain";
        Editor result = Editor.createEditorForExtension(extension, app);
        assertEquals(expResult, result.getContentType());
        
        extension = "html";
        expResult = "text/html";
        result = Editor.createEditorForExtension(extension, app);
        assertEquals(expResult, result.getContentType());
        
        extension = "htm";
        expResult = "text/html";
        result = Editor.createEditorForExtension(extension, app);
        assertEquals(expResult, result.getContentType());
        
        extension = "rtf";
        expResult = "text/rtf";
        result = Editor.createEditorForExtension(extension, app);
        assertEquals(expResult, result.getContentType());
        
        extension = "text";
        expResult = "text/plain";
        result = Editor.createEditorForExtension(extension, app);
        assertEquals(expResult, result.getContentType());
        
        extension = "txt";
        expResult = "text/plain";
        result = Editor.createEditorForExtension(extension, app);
        assertEquals(expResult, result.getContentType());
    }

    /**
     * Test of getFile method, of class Editor.
     */
    @Test
    public void testGetFile() {
        System.out.println("getFile");
        Editor instance = new TEXTEditor(eam);
        File expResult = null;
        File result = instance.getFile();
        assertEquals(expResult, result);
    }

    /**
     * Test of getFileExtensions method, of class Editor.
     */
    @Test
    public void testGetFileExtensions() {
        System.out.println("getFileExtensions");
        Editor instance = new TEXTEditor(eam);
        String[] expResult = null;
        String[] result = instance.getFileExtensions();
        assertNotNull(result);
    }

    /**
     * Test of getInputAttributes method, of class Editor.
     */
    @Test
    public void testGetInputAttributes() {
        System.out.println("getInputAttributes");
        Editor instance = new TEXTEditor(eam);
        MutableAttributeSet expResult = new SimpleAttributeSet();
        MutableAttributeSet result = instance.getInputAttributes();
        assertEquals(expResult, result);
    }

    /**
     * Test of getPopup method, of class Editor.
     */
    @Test
    public void testGetPopup() {
        System.out.println("getPopup");
        Editor instance = new TEXTEditor(eam);
        JPopupMenu result = instance.getPopup();
        assertNotNull(result);
    }

    /**
     * Test of getRegistry method, of class Editor.
     */
    @Test
    public void testGetRegistry() {
        System.out.println("getRegistry");
        Editor instance = new TEXTEditor(eam);
        Registry result = instance.getRegistry();
        assertNotNull(result);
    }

    /**
     * Test of getSimpleAttributeSet method, of class Editor.
     */
    @Test
    public void testGetSimpleAttributeSet() {
        System.out.println("getSimpleAttributeSet");
        Editor instance = new TEXTEditor(eam);
        MutableAttributeSet expResult = new SimpleAttributeSet();
        MutableAttributeSet result = instance.getSimpleAttributeSet();
        assertEquals(expResult, result);
    }

    /**
     * Test of getTextComponent method, of class Editor.
     */
    @Test
    public void testGetTextComponent() {
        System.out.println("getTextComponent");
        Editor instance = new TEXTEditor(eam);
        JEditorPane result = instance.getTextComponent();
        assertNotNull(result);
    }

    /**
     * Test of getUndoManager method, of class Editor.
     */
    @Test
    public void testGetUndoManager() {
        System.out.println("getUndoManager");
        Editor instance = new TEXTEditor(eam);
        UndoManager result = instance.getUndoManager();
        assertNotNull(result);
    }

    /**
     * Test of hasBeenActivated method, of class Editor.
     */
    @Test
    public void testHasBeenActivated() {
        System.out.println("hasBeenActivated");
        Editor editor = null;
        HTMLEditor instance = new HTMLEditor(eam);
        try{
            instance.hasBeenActivated(editor);
        } catch(Exception e){
            fail("Exception thrown");
        }
        File file = new File("test.txt");
        instance.setFile(file);
        editor = null;
        try{
            instance.hasBeenActivated(editor);
        } catch(Exception e){
            fail("Exception thrown");
        }
    }

    /**
     * Test of hasBeenDeactivated method, of class Editor.
     */
    @Test
    public void testHasBeenDeactivated() {
        System.out.println("hasBeenDeactivated");
        Editor editor = null;
        HTMLEditor instance = new HTMLEditor(eam);
        instance.hasBeenDeactivated(editor);
        editor = instance;
        instance.hasBeenDeactivated(editor);
    }

    /**
     * Test of hasFile method, of class Editor.
     */
    @Test
    public void testHasFile() {
        System.out.println("hasFile");
        Editor instance = new HTMLEditor(eam);
        boolean expResult = false;
        boolean result = instance.hasFile();
        assertEquals(expResult, result);
        File file = new File("test.txt");
        expResult = true;
        instance.setFile(file);
        result = instance.hasFile();
        assertEquals(expResult, result);
    }

    /**
     * Test of insert method, of class Editor.
     */
    @Test
    public void testInsert() throws Exception {
        System.out.println("insert");
        File file = null;
        int position = 0;
        Editor instance = new RTFEditor(eam);
        try{
            instance.insert(file, position);
        } catch(IOException e){
            System.out.println("Approriate exception caught");
        } catch(Exception e){
            fail("Caught incorrect exception");
        }
        position = -1;
        //file = new File("text.txt");
        try{
            instance.insert(file, position);
        } catch(IOException e){
            System.out.println("Approriate exception caught");
        } catch(Exception e){
            fail("Caught incorrect exception");
        }
    }

    /**
     * Test of isChanged method, of class Editor.
     */
    @Test
    public void testIsChanged() {
        System.out.println("isChanged");
        Editor instance = new RTFEditor(eam);
        boolean expResult = false;
        boolean result = instance.isChanged();
        assertEquals(expResult, result);
    }

    /**
     * Test of isEmpty method, of class Editor.
     */
    @Test
    public void testIsEmpty() {
        System.out.println("isEmpty");
        Editor instance = new TEXTEditor(eam);
        boolean expResult = true;
        boolean result = instance.isEmpty();
        assertEquals(expResult, result);
    }

    /**
     * Test of isFormatted method, of class Editor.
     */
    @Test
    public void testIsFormatted() {
        System.out.println("isFormatted");
        Editor instance = new RTFEditor(eam);
        boolean expResult = false;
        boolean result = instance.isFormatted();
        assertEquals(expResult, result);
    }

    /**
     * Test of isNew method, of class Editor.
     */
    @Test
    public void testIsNew() {
        System.out.println("isNew");
        Editor instance = new RTFEditor(eam);
        boolean expResult = true;
        boolean result = instance.isNew();
        assertEquals(expResult, result);
    }

    /**
     * Test of read method, of class Editor.
     */
    @Test
    public void testRead() throws Exception {
        System.out.println("read");
        File file = null;
        Editor instance = new RTFEditor(eam);
        try{
            instance.read(file);
        } catch(IOException e){
            System.out.println("Approriate exception caught");
        } catch(Exception e){
            fail("Caught incorrect exception");
        }
    }

    /**
     * Test of setChanged method, of class Editor.
     */
    @Test
    public void testSetChanged() {
        System.out.println("setChanged");
        boolean changed = false;
        Editor instance = new RTFEditor(eam);
        instance.setChanged(changed);
        boolean expResult = false;
        assertEquals(expResult, instance.isChanged());
        
        changed = true;
        instance.setChanged(changed);
        expResult = true;
        assertEquals(expResult, instance.isChanged());
    }

    /**
     * Test of setCurrentParagraph method, of class Editor.
     */
    @Test
    public void testSetCurrentParagraph() {
        System.out.println("setCurrentParagraph");
        Element paragraph = null;
        Editor instance = new RTFEditor(eam);
        instance.setCurrentParagraph(paragraph);
        assertNull(instance.getCurrentParagraph());
    }

    /**
     * Test of setCurrentRun method, of class Editor.
     */
    @Test
    public void testSetCurrentRun() {
        System.out.println("setCurrentRun");
        Element run = null;
        Editor instance = new RTFEditor(eam);
        assertNull(instance.getCurrentRun());
    }

    /**
     * Test of setFile method, of class Editor.
     */
    @Test
    public void testSetFile() {
        System.out.println("setFile");
        File file = null;
        Editor instance = new RTFEditor(eam);
        instance.setFile(file);
        assertNull(instance.getFile());
    }

    /**
     * Test of write method, of class Editor.
     */
    @Test
    public void testWrite() throws Exception {
        System.out.println("write");
        File file = null;
        Editor instance = new RTFEditor(eam);
        try{
            instance.write(file);
        } catch(IOException e){
            System.out.println("Approriate exception caught");
        } catch(Exception e){
            fail("Caught incorrect exception");
        }
    }
}
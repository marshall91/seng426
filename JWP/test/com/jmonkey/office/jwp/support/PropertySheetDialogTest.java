/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jmonkey.office.jwp.support;

import com.jmonkey.office.jwp.JWP;
import java.awt.Frame;
import java.util.Properties;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author mackenziemarshall
 */
public class PropertySheetDialogTest {
    JWP app;
    
    public PropertySheetDialogTest() throws Exception{
        app = new JWP(null);
    }


    /**
     * Test of display method, of class PropertySheetDialog.
     */
    @Test
    public void testDisplay_Frame_Properties() {
        System.out.println("display");
        Frame parent = app;
        Properties p = app.getRegistry().referenceGroup("USER");
        Properties result = PropertySheetDialog.display(parent, p);
        assertNotNull(result);
    }

    /**
     * Test of display method, of class PropertySheetDialog.
     */
    @Test
    public void testDisplay_3args() {
        System.out.println("display");
        Frame parent = app;
        Properties p = app.getRegistry().referenceGroup("USER");
        boolean allowAdd = false;
        Properties result = PropertySheetDialog.display(parent, p, allowAdd);
        assertNotNull(result);
        allowAdd=true;
        result = PropertySheetDialog.display(parent, p, allowAdd);
        assertNotNull(result);
    }
}
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jmonkey.office.jwp;

import com.jmonkey.export.RegistryFormatException;
import java.util.Properties;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author tri
 */
public class ColourPropertySheetTest {
    
    private JWP app;
    private Properties p;
    private boolean allowAdd;
    
    public ColourPropertySheetTest() throws RegistryFormatException {
        app = new JWP(null);
        p = new Properties();
    }


    /**
     * Test of getProperties method, of class ColourPropertySheet.
     */
    @Test
    public void testGetProperties() {
        System.out.println("getProperties");
        ColourPropertySheet instance = new ColourPropertySheet(app,p,allowAdd);
        Properties result = instance.getProperties();
        assertNotNull(result);
    }
}

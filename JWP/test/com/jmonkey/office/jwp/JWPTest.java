/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jmonkey.office.jwp;

import com.jmonkey.export.Registry;
import com.jmonkey.export.RegistryFormatException;
import com.jmonkey.office.jwp.support.EditorActionManager;
import java.io.File;
import java.lang.reflect.Field;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Action;
import javax.swing.JMenu;
import javax.swing.JToolBar;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Michael
 */
public class JWPTest {
    private static final String TEST_DIR = "test\\com\\jmonkey\\office\\jwp\\";
    
    public JWPTest() {
        createJWP(null);
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    
    private JWP createJWP(String[] args) {
        JWP app = null;
        try {
            app = new JWP(args);
        } catch (RegistryFormatException ex) {
            Logger.getLogger(JWPTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return app;
    }

    /**
     * Test of getDesktop method, of class JWP.
     */
    @Test
    public void testGetDesktop() {
        System.out.println("getDesktop");
        assertNotNull(JWP.getDesktop());
    }

    /**
     * Test of getResources method, of class JWP.
     */
    @Test
    public void testGetResources() {
        System.out.println("getResources");
        assertNotNull(JWP.getResources());
    }

    /**
     * Test of getMessage method, of class JWP.
     */
    @Test
    public void testGetMessage() {
        System.out.println("getMessage");
        
        String result = JWP.getMessage("dialog.open.title");
        assertNotNull(result);
        
        try {
            result = JWP.getMessage("not a key");
        } catch (Exception ex) {
            result = null;
        }
        
        assertNull(result);
    }

    /**
     * Test of switchedDocumentFrame method, of class JWP.
     */
    @Test
    public void testSwitchedDocumentFrame() {
        System.out.println("switchedDocumentFrame");
        boolean textSelected = false;
        JWP instance = createJWP(null);
        DocumentFrame frame = new DocumentFrame(instance, "text/rtf");
        
        frame.setTitle("Test");
        frame.getEditor().setChanged(false);
        instance.switchedDocumentFrame(frame, textSelected);
        assertEquals(instance.getTitle(), "Main - [Test]");
        
        frame.getEditor().setChanged(true);
        instance.switchedDocumentFrame(frame, textSelected);
        assertEquals(instance.getTitle(), "Main - [Test] *");
    }

    /**
     * Test of actionPerformed method, of class JWP.
     */
    @Test
    public void testActionPerformed() {
        System.out.println("actionPerformed");
        // ActionPerformed method should be removed.
        // Currently exits the program.
    }

    /**
     * Test of addToFileHistory method, of class JWP.
     */
    @Test
    public void testAddToFileHistory() {
        System.out.println("addToFileHistory");
        File file = new File(TEST_DIR + "test.txt");
        JWP instance = createJWP(null);
        
        // Insert a single file.
        instance.addToFileHistory(file);
        assertEquals(instance.m_fileHistory.getItem(0).getText(), file.getName());
        assertEquals(instance.m_fileHistory.getItem(0).getActionCommand(), file.getAbsolutePath());
        
        // Insert more than the maximum number of files
        // allowed to be stored.
        file = new File(TEST_DIR + "test2.txt");
        int maxNumFiles = instance.getRegistry().getInteger("USER", "max.file.history");
        for (int i = 0; i < maxNumFiles; i++) {
            instance.addToFileHistory(file);
        }
        
        assertEquals(instance.m_fileHistory.getItem(0).getText(), file.getName());
        assertEquals(instance.m_fileHistory.getItem(0).getActionCommand(), file.getAbsolutePath());
        
        // Try null arguments.
        try {
            instance.addToFileHistory(null);
        } catch (IllegalArgumentException e) {
            // Acceptable exception.
        } catch (Exception e) {
            fail("Incorrect exception thrown.");
        }
    }

    /**
     * Test of doExit method, of class JWP.
     */
    @Test
    public void testDoExit() {
        System.out.println("doExit");
        // Exits the application. Must be tested manually.
    }

    /**
     * Test of getDesktopManager method, of class JWP.
     */
    @Test
    public void testGetDesktopManager() {
        System.out.println("getDesktopManager");
        JWP instance = createJWP(null);
        DocumentManager result = instance.getDesktopManager();
        assertNotNull(result);
        assert(result instanceof DocumentManager);
    }

    /**
     * Test of getEditorActionManager method, of class JWP.
     */
    @Test
    public void testGetEditorActionManager() {
        System.out.println("getEditorActionManager");
        JWP instance = createJWP(null);
        EditorActionManager result = instance.getEditorActionManager();
        assertNotNull(result);
        assert(result instanceof EditorActionManager);
    }

    /**
     * Test of getDocumentNumber method, of class JWP.
     */
    @Test
    public void testGetDocumentNumber() {
        System.out.println("getDocumentNumber");
        JWP instance = createJWP(null);
        
        // Reset the count field back to zero.
        Field count;
        try {
            count = JWP.class.getDeclaredField("count");
            count.setAccessible(true);
            count.set(instance, 0);
        } catch (Exception e) {
            fail(e.getMessage());
            return;
        }
        
        // Check that is being incremented correctly.
        assert(instance.getDocumentNumber() == 0);
        assert(instance.getDocumentNumber() == 1);
        
        // Check overflow.
        try {
            count.set(instance, Integer.MAX_VALUE);
        } catch (Exception e) {
            fail(e.getMessage());
        }
        
        assert(instance.getDocumentNumber() == 0);
    }

    /**
     * Test of getFileToolBar method, of class JWP.
     */
    @Test
    public void testGetFileToolBar() {
        System.out.println("getFileToolBar");
        JWP instance = createJWP(null);
        JToolBar result = instance.getFileToolBar();
        assertNotNull(result);
        assert(result instanceof JToolBar);
    }

    /**
     * Test of getFormatToolBar method, of class JWP.
     */
    @Test
    public void testGetFormatToolBar() {
        System.out.println("getFormatToolBar");
        JWP instance = createJWP(null);
        JToolBar result = instance.getFormatToolBar();
        assertNotNull(result);
        assert(result instanceof JToolBar);
    }

    /**
     * Test of getFontSizes method, of class JWP.
     */
    @Test
    public void testGetFontSizes() {
        System.out.println("getFontSizes");
        JWP instance = createJWP(null);
        int[] result = instance.getFontSizes();
        assertNotNull(result);
        assert(result.length > 0);
        
        for (int i = 0; i < result.length; i++) {
            assertNotNull(result[i]);
        }
    }

    /**
     * Test of getHELPAction method, of class JWP.
     */
    @Test
    public void testGetHELPAction() {
        System.out.println("getHELPAction");
        JWP instance = createJWP(null);
        Action result = instance.getHELPAction();
        assertNotNull(result);
        assert(result instanceof JWP.HELPAction);
    }

    /**
     * Test of getMain method, of class JWP.
     */
    @Test
    public void testGetMain() {
        System.out.println("getMain");
        JWP instance = createJWP(null);
        JWP result = instance.getMain();
        assertEquals(result, instance);
    }

    /**
     * Test of getPrintAction method, of class JWP.
     */
    @Test
    public void testGetPrintAction() {
        System.out.println("getPrintAction");
        JWP instance = createJWP(null);
        Action result = instance.getPrintAction();
        assertNotNull(result);
        assert(result instanceof JWP.PrintAction);
    }

    /**
     * Test of getRegistry method, of class JWP.
     */
    @Test
    public void testGetRegistry() {
        System.out.println("getRegistry");
        JWP instance = createJWP(null);
        Registry result = instance.getRegistry();
        assertNotNull(result);
        
        assert(result.isGroup("USER"));
        assert(result.isGroup("MAIN"));
        assert(result.isGroup("OPTION"));
        assert(result.isGroup("FONTS"));
        assert(result.isGroup("COLOURS"));
        assert(result.isGroup("POPUP"));
        
        try {
            result.deleteAll();
            result.commit();
        
            Field registry = JWP.class.getDeclaredField("m_mainRegistry");
            registry.setAccessible(true);
            registry.set(instance, null);
        } catch (Exception e) {
            fail(e.getMessage());
        }
        
        result = instance.getRegistry();
        
        assert(result.isGroup("USER"));
        assert(result.isGroup("MAIN"));
        assert(result.isGroup("OPTION"));
        assert(result.isGroup("FONTS"));
        assert(result.isGroup("COLOURS"));
        assert(result.isGroup("POPUP"));
    }

    /**
     * Test of getStatusLabel method, of class JWP.
     */
    @Test
    public void testGetStatusLabel() {
        System.out.println("getStatusLabel");
        JWP instance = createJWP(null);
        assertNotNull(instance.getStatusLabel());
    }

    /**
     * Test of getUpdateAction method, of class JWP.
     */
    @Test
    public void testGetUpdateAction() {
        System.out.println("getUpdateAction");
        JWP instance = createJWP(null);
        Action action = instance.getUpdateAction();
        assertNotNull(action);
        assert(action instanceof JWP.UpdateAction);
    }

    /**
     * Test of main method, of class JWP.
     */
    @Test
    public void testMain() {
        System.out.println("main");
        String[] args = new String[] { "test\\com\\jmonkey\\export\\testWriter.txt" };
        JWP.main(args);
        JWP.MainDesktop desktop = JWP.getDesktop();
        String[] openDocs = ((DocumentManager)desktop.getDesktopManager()).openDocumentList();
        assert(openDocs.length > 0);
        assertEquals(openDocs[1], "testWriter.txt [text/plain]");
    }

    /**
     * Test of updateOpenWindowsMenu method, of class JWP.
     */
    @Test
    public void testUpdateOpenWindowsMenu() {
        System.out.println("updateOpenWindowsMenu");
        JWP instance = createJWP(null);
        JWP.MainDesktop desktop = JWP.getDesktop();
        String[] openDocs = ((DocumentManager)desktop.getDesktopManager()).openDocumentList();
        instance.updateOpenWindowsMenu();
        
        try {
            Field openWindowsField = JWP.class.getDeclaredField("m_openWindows");
            openWindowsField.setAccessible(true);
            JMenu openWindows = (JMenu) openWindowsField.get(instance);
            assert(openWindows.getItemCount() == openDocs.length);
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }
}
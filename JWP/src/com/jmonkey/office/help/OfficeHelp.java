package com.jmonkey.office.help;

import java.awt.BorderLayout;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

import com.jmonkey.office.jwp.support.Code;

import com.jmonkey.office.jwp.support.images.Loader;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JEditorPane;

public class OfficeHelp extends JFrame {
  // static URL backURL = null;
  // static URL forURL = null;
  // htmlFrame hFrame;
  private String start_loc = null;

  // protected Browser iceBrowser;
  private Class iceClass;

  private JComponent iceBrowser;

  public OfficeHelp(String helpFile) {
    super("Program Help");
    setIconImage(Loader.load("help_book16.gif"));
    getContentPane().setLayout(new BorderLayout());
    start_loc = "http://www.jmonkey.com/~mschmidt/help/" + helpFile
        + "/index.html";
    try {
        iceBrowser = (JComponent) new JEditorPane(start_loc);
    } catch (IOException ex) {
    }
    this.getContentPane().add("Center", iceBrowser);
  }
  
}
